package other;


import fdareader.MainPanel;
import java.io.File;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author tappret
 */
public class MoulinetteReverseData {

    public MoulinetteReverseData(String path) {
        MainPanel main = new MainPanel();
        File directory = new File(path);
        File[] listFile = directory.listFiles();

        for (File file : listFile) {
            if (!file.isDirectory()) {
                File newFile;
                newFile = new File(file.getParent() + "/FileConverted/" + file.getName());
                System.out.println(newFile.getAbsolutePath());
                main.loadFile(file.getPath());
                String fileName = file.getName();
                int numberOfSepared = fileName.split("_").length;
                String comment = "";
                if(numberOfSepared == 5){
                    comment = fileName.substring(fileName.lastIndexOf("_")+1, fileName.length() - 4);
                }
                System.out.println("Comment:" + comment);
                    
                
                main.save(comment, newFile.getAbsolutePath());
                
            }
        }

    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */

        /* Create and display the form */
        MoulinetteReverseData main = new MoulinetteReverseData("/mcsvar/FdaFiles/");

    }
}
