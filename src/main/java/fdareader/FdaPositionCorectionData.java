/*
 * To change this license header", choose License Headers in Project Properties.
 * To change this template file", choose Tools | Templates
 * and open the template in the editor.
 */
package fdareader;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 *
 * @author tappret
 */
public class FdaPositionCorectionData {
    FdaData global;
        
    int nbByteRead;
    
    double[] modulus;


    PlanView horizontal;
    PlanView vertical;
    
    
    int nbIds;


    public FdaPositionCorectionData(FdaData globalData,int nbIds) {
        this.global = globalData;
        this.nbIds = nbIds;

        horizontal = new PlanView(globalData,this, true);
        vertical = new PlanView(globalData,this, false);
    }

    

    public void computeAverage() {
        //long start = System.currentTimeMillis();
        horizontal.computeAverage();
        vertical.computeAverage();
        //System.out.println("time spende:" + (System.currentTimeMillis() - start));
    }

    /**
     * Performs FFT of the input signal (Requires constant x intervals)
     *
     * @param tab
     * @param index
     * @param removeAverage
     * @throws fdareader.FdaException
     */
    public void doFFT(double[][] tab, int index, boolean removeAverage) throws FdaException {

        int nbSampl = tab.length;
        if (nbSampl < FdaConstants.NB_SAMPLE_FFT) {
            throw new FdaException("not enough samples");
        }
        int p;
        int i, idx;
        // Get the power of 2 above
        if ((nbSampl & (nbSampl - 1)) == 0) {
            p = -1; // already a power of 2
        } else {
            p = 0;
        }
        while (nbSampl != 0) {
            nbSampl = nbSampl >> 1;
            p++;
        }
        nbSampl = 1 << p;

        double avg = 0.0;
        if (removeAverage) {
            // Compute average
            for (i = 0; i < tab.length; i++) {
                avg += tab[i][index];
            }
            avg = avg / (double) tab.length;
        }

        // Create initial array
        double[] real = new double[nbSampl];
        double[] imag = new double[nbSampl];
        for (i = 0; i < tab.length; i++) {
            idx = reverse(i, p);
            real[idx] = tab[i][index] - avg;
            imag[idx] = 0.0;
        }
        for (; i < nbSampl; i++) {
            idx = reverse(i, p);
            real[idx] = 0.0;
            imag[idx] = 0.0;
        }

        // Do the FFT
        int blockEnd = 1;
        for (int blockSize = 2; blockSize <= nbSampl; blockSize <<= 1) {

            double deltaAngle = 2.0 * Math.PI / (double) blockSize;

            double sm2 = Math.sin(-2.0 * deltaAngle);
            double sm1 = Math.sin(-deltaAngle);
            double cm2 = Math.cos(-2.0 * deltaAngle);
            double cm1 = Math.cos(-deltaAngle);
            double w = 2.0 * cm1;
            double ar0, ar1, ar2, ai0, ai1, ai2;

            for (i = 0; i < nbSampl; i += blockSize) {
                ar2 = cm2;
                ar1 = cm1;

                ai2 = sm2;
                ai1 = sm1;

                for (int j = i, n = 0; n < blockEnd; j++, n++) {
                    ar0 = w * ar1 - ar2;
                    ar2 = ar1;
                    ar1 = ar0;

                    ai0 = w * ai1 - ai2;
                    ai2 = ai1;
                    ai1 = ai0;

                    int k = j + blockEnd;
                    double tr = ar0 * real[k] - ai0 * imag[k];
                    double ti = ar0 * imag[k] + ai0 * real[k];

                    real[k] = real[j] - tr;
                    imag[k] = imag[j] - ti;

                    real[j] += tr;
                    imag[j] += ti;
                }
            }

            blockEnd = blockSize;
        }

        // Create modulus of arguments results
        double nS = (double) nbSampl;

        //int nbPointToCalc = (int)(1 / MainPanel.SAMPLE_FREQUENCY) * tab.length;
        int nbPointToCalc = (int) (FdaConstants.NB_SAMPLE_FFT / FdaConstants.SAMPLE_FREQUENCY * nS);

        global.frequenciesFFT = new double[nbPointToCalc];
        modulus = new double[nbPointToCalc];
        for (i = 0; i < nbPointToCalc; i++) {
            modulus[i] = (Math.sqrt(real[i] * real[i] + imag[i] * imag[i])) / nS;
            global.frequenciesFFT[i] = (double) i * FdaConstants.SAMPLE_FREQUENCY / nS;
        }
    }

    public int getNbPointForDoFFTTo(double maxFrequency) {
        int nbSampl = global.nbSample;

        int p;
        // Get the power of 2 above
        if ((nbSampl & (nbSampl - 1)) == 0) {
            p = -1; // already a power of 2
        } else {
            p = 0;
        }
        while (nbSampl != 0) {
            nbSampl = nbSampl >> 1;
            p++;
        }
        nbSampl = 1 << p;

        return (int) (FdaConstants.NB_SAMPLE_FFT / FdaConstants.SAMPLE_FREQUENCY * nbSampl);
    }

    public double[] getModulus() {
        return modulus;
    }

    public double[] getFrequencies() {
        return global.frequenciesFFT;
    }

    /* Reverse bit of idx on p bits */
    private int reverse(int idx, int p) {
        int i, rev;
        for (i = rev = 0; i < p; i++) {
            rev = (rev << 1) | (idx & 1);
            idx >>= 1;
        }
        return rev;
    }

    PlanView getPlan(boolean horizontalOrVertical) {
        if (horizontalOrVertical) {
            return this.horizontal;
        } else {
            return this.vertical;
        }

    }

    void writeZipDescription(ZipOutputStream zos) {
        String headerData;
        headerData = "NB_ID:" + nbIds + "\r\n";
        headerData += "NB_SAMPLE:" + global.nbSample + "\r\n";
        headerData += "DECIMATE_MODE:" + global.decimateMode + "\r\n";
        headerData += "TIMESTAMP_ZERO:" + global.timeStamp[0] + "\r\n";
        headerData += "SAMPLE_FREQUENCY:" + FdaConstants.SAMPLE_FREQUENCY + "\r\n";
        headerData += "DURATION:" + global.duration + "\r\n";

        try {
            zos.putNextEntry(new ZipEntry("description"));

            byte[] buff = headerData.getBytes();
            zos.write(buff);

        } catch (IOException ex) {
            Logger.getLogger(FdaPositionCorectionData.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    int nbByteToWrite = -1;

    int getNbByteToWrite() {
        if (global.decimateMode == FdaConstants.DecimateMode.FULL) {
            nbByteToWrite = global.nbSample * 4 * nbIds * 2;
        } else {
            nbByteToWrite = global.nbSample * 4 * nbIds * 2 * 4;
        }
        return nbByteToWrite;
    }

    int writeZip(ZipOutputStream zos, int nbByteAlreadyWrite, int nbByteTotal, ThreadDlg progress) {
        try {
            double correction_uAto_mA = 1;
            
            switch (this.nbIds) {
                case FdaConstants.NB_BPM:
                    zos.putNextEntry(new ZipEntry("position"));
                    break;
                case FdaConstants.NB_CORRESTOR:
                    zos.putNextEntry(new ZipEntry("corrector"));
                    correction_uAto_mA = FdaConstants.CONVERTION_FACTOR_FOR_CORRECTOR;
                    break;
                default:
                    zos.putNextEntry(new ZipEntry("current"));
                    break;
            }

            ByteBuffer bb = ByteBuffer.allocate(nbByteToWrite);
            bb.clear();

            if (global.decimateMode == FdaConstants.DecimateMode.FULL) {
                
                for (int j = 0; j < global.nbSample; j++) {
                    for (int i = 0; i < nbIds; i++) {
                        bb.putInt((int) (horizontal.mean[j][i]/correction_uAto_mA));
                        bb.putInt((int) (vertical.mean[j][i]/correction_uAto_mA));
                    }
                }
            } else {
                for (int j = 0; j < global.nbSample; j++) {
                    for (int i = 0; i < nbIds; i++) {
                        bb.putInt((int) (horizontal.mean[j][i]/correction_uAto_mA));
                        bb.putInt((int) (vertical.mean[j][i]/correction_uAto_mA));

                        bb.putInt((int) (horizontal.stdDev[j][i]/correction_uAto_mA));
                        bb.putInt((int) (vertical.stdDev[j][i]/correction_uAto_mA));

                        bb.putInt((int) (horizontal.min[j][i]/correction_uAto_mA));
                        bb.putInt((int) (vertical.min[j][i]/correction_uAto_mA));

                        bb.putInt((int) (horizontal.max[j][i]/correction_uAto_mA));
                        bb.putInt((int) (vertical.max[j][i]/correction_uAto_mA));
                    }
                }
            }

            int lengthBuff = 1024 * 1024;
            byte[] bigBuff = bb.array();
            
            int nbLoop = bigBuff.length / lengthBuff;
            int restBuff = bigBuff.length % lengthBuff;
            for (int i = 0; i < nbLoop + 1; i++) {
                if (i == nbLoop) {
                    zos.write(bigBuff, i * lengthBuff, restBuff);
                } else {
                    zos.write(bigBuff, i * lengthBuff, lengthBuff);
                }
                progress.setProgress(((double) (nbByteAlreadyWrite + (i * lengthBuff)) / nbByteTotal), null);
            }
        } catch (IOException ex) {
            Logger.getLogger(FdaPositionCorectionData.class.getName()).log(Level.SEVERE, null, ex);
        }
        return nbByteToWrite;
    }

    static FdaPositionCorectionData readZipEntry(FdaData globalData,BufferedInputStream zis, ZipEntry entry, int nbByteAlreadyRead, long nbByteTotal, ThreadDlg progress) {
        String nameEntry = entry.getName();
        int nbId = 1;
        double correction_uAto_mA = 1;
        if (null != nameEntry) switch (nameEntry) {
            case "position":
                nbId = FdaConstants.NB_BPM;
                break;
                //to read the old file with the wrong name
            case "correction":
            case "corrector":
                nbId = FdaConstants.NB_CORRESTOR;
                correction_uAto_mA = FdaConstants.CONVERTION_FACTOR_FOR_CORRECTOR;
                break;
            default:
                break;
        }


        FdaPositionCorectionData data = new FdaPositionCorectionData(globalData,nbId);

        int lengthBuff = 1024 * 1024;
        byte[] bigBuff = new byte[(int) entry.getSize()];

        int nbLoop = bigBuff.length / lengthBuff;
        int restBuff = bigBuff.length % lengthBuff;
        try {
            for (int i = 0; i < nbLoop + 1; i++) {
                if (i == nbLoop) {
                    zis.read(bigBuff, i * lengthBuff, restBuff);
                } else {
                    zis.read(bigBuff, i * lengthBuff, lengthBuff);
                }
                progress.setProgress(((double) (nbByteAlreadyRead + (i * lengthBuff)) / nbByteTotal), null);
            }
            data.nbByteRead = bigBuff.length;
        } catch (IOException ex) {
            Logger.getLogger(FdaPositionCorectionData.class.getName()).log(Level.SEVERE, null, ex);
        }
        

        ByteBuffer bb = ByteBuffer.allocate((int) entry.getSize());
        bb.put(bigBuff);
        bb.flip();

        if (globalData.decimateMode == FdaConstants.DecimateMode.FULL) {
            for (int j = 0; j < globalData.nbSample; j++) {
                for (int i = 0; i < data.nbIds; i++) {
                    data.horizontal.mean[j][i] = ((double) bb.getInt()) * correction_uAto_mA;
                    data.vertical.mean[j][i] = ((double) bb.getInt()) * correction_uAto_mA;
                }
            }
        } else {
            for (int j = 0; j < globalData.nbSample; j++) {
                for (int i = 0; i < data.nbIds; i++) {
                    try {
                        data.horizontal.mean[j][i] = ((double) bb.getInt()) * correction_uAto_mA;
                        data.vertical.mean[j][i] = ((double) bb.getInt()) * correction_uAto_mA;

                        data.horizontal.stdDev[j][i] = ((double) bb.getInt()) * correction_uAto_mA;
                        data.vertical.stdDev[j][i] = ((double) bb.getInt()) * correction_uAto_mA;

                        data.horizontal.min[j][i] = ((double) bb.getInt()) * correction_uAto_mA;
                        data.vertical.min[j][i] = ((double) bb.getInt()) * correction_uAto_mA;

                        data.horizontal.max[j][i] = ((double) bb.getInt()) * correction_uAto_mA;
                        data.vertical.max[j][i] = ((double) bb.getInt()) * correction_uAto_mA;
                    } catch (Exception e) {
                        Logger.getLogger(FdaPositionCorectionData.class.getName()).log(Level.SEVERE, null, e);
                    }
                }
            }

        }

        data.computeAverage();

        return data;
    }

}
