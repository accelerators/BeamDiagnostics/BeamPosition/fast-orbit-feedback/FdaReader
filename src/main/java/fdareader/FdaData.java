/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fdareader;

import static fdareader.FdaConstants.getSampleDeltaTime;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Enumeration;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import javax.swing.JOptionPane;

/**
 *
 * @author tappret
 */
public class FdaData {
    private final static String TIMESTAMP_ZERO = "TIMESTAMP_ZERO:";
    
    boolean meanOrStdDev;
    boolean fft;
    boolean ac;
    boolean min;
    boolean max;
    boolean viewCurrent;
    
    int nbSample = 0;
    int duration;
    
    FdaConstants.TimeStampMode timeStampMode;
    FdaConstants.DecimateMode decimateMode;
    FdaConstants.DataTypeRequest dataTypeReq;
    
    double[] frequenciesFFT;
    double[] frequencies;
    double[] timeStamp;
    
    FdaPositionCorectionData position;
    FdaPositionCorectionData correction;
    FdaPositionCorectionData current;
    
    double timeStart;
    double timeStop;
    
    
    public void setMeanOrStdDevTmp(boolean meanOrStdDevTmp) {
        this.meanOrStdDev = meanOrStdDevTmp;
    }

    public void setFft(boolean fft) {
        this.fft = fft;
    }

    public void setAc(boolean ac) {
        this.ac = ac;
    }

    public void setMax(boolean max) {
        this.max = max;
    }

    public void setMin(boolean min) {
        this.min = min;
    }
    
    void setViewCurrent(boolean viewCurrent) {
        this.viewCurrent = viewCurrent;
    }
    
    
    static FdaData loadFile(String file, ThreadDlg progress) throws FdaException {
        long time0 = System.currentTimeMillis();

        //String fileName = MainPanel.PATH_OF_SAVE + "" + position.timeStamp[0] + "_" + position.decimateMode + "_"+ position.nbSample +"_" + position.getDuration();
        FdaData data = new FdaData();
        //FdaPositionCorectionData[] data = new FdaPositionCorectionData[2];

        File f = new File(file);

        try{
            ZipFile zip = new ZipFile(file);
            Enumeration<ZipEntry> listEntry = (Enumeration<java.util.zip.ZipEntry>) zip.entries();
            
            ZipEntry descriptionZipEntry = null, positionZipEntry = null, correctionZipEntry = null, currentZipEntry = null;
            
            while(listEntry.hasMoreElements()){
                ZipEntry tmp = listEntry.nextElement();
                switch(tmp.getName()){
                    case "current":
                        currentZipEntry = tmp;
                        break;
                    case "position":
                        positionZipEntry = tmp;
                        break;
                    //to read the old file with the wrong name
                    case "correction":
                    case "corrector":
                        correctionZipEntry = tmp;
                        break;
                    case "description":
                        descriptionZipEntry = tmp;
                        break;
                }
            }
            
            if (positionZipEntry == null || correctionZipEntry == null || descriptionZipEntry == null) {
                throw new FdaException("Invalide zip file!!! This file do not contain position et correction file.");
            }
            
            long nbByteTotal = positionZipEntry.getSize();
            //zisCheck.skip(nbByteTotal);
            nbByteTotal += correctionZipEntry.getSize();
            //zisCheck.getNextEntry();
            //zisCheck.closeEntry();
            //zisCheck.close();
            BufferedInputStream zis = new BufferedInputStream(zip.getInputStream(descriptionZipEntry));
            byte [] buf = new byte[(int)descriptionZipEntry.getSize()];
            
            zis.read(buf);
            
            //parse file name to get some info
            String header = new String(buf);
            
            String[] path = file.split("/");
            file = path[path.length - 1];

            String[] tab = file.split("_");
            data.nbSample = Integer.parseInt(tab[2]);
            data.decimateMode = FdaConstants.DecimateMode.valueOf(tab[1]);
            data.meanOrStdDev = (data.decimateMode == FdaConstants.DecimateMode.FULL);
            data.duration = (int)findLableReadDouble("DURATION:",header);
            
            if(currentZipEntry != null){
                zis = new BufferedInputStream(zip.getInputStream(currentZipEntry));
                
                data.current = FdaPositionCorectionData.readZipEntry(data,zis,currentZipEntry, 0, nbByteTotal, progress);
            }
            
            zis = new BufferedInputStream(zip.getInputStream(positionZipEntry));
            //System.out.println(zis.available());
            
            data.position = FdaPositionCorectionData.readZipEntry(data,zis,positionZipEntry, 0, nbByteTotal, progress);
            
            zis = new BufferedInputStream(zip.getInputStream(correctionZipEntry));
            
            //System.out.println(zis.available());
            
            data.correction = FdaPositionCorectionData.readZipEntry(data,zis,correctionZipEntry, data.position.nbByteRead, nbByteTotal, progress);
            
            double sampleDeltaTime = getSampleDeltaTime(data.decimateMode);
            
            double t0 = findLableReadDouble(TIMESTAMP_ZERO,header);
            
            data.timeStamp = new double[data.nbSample];
            data.timeStamp[0] = t0;
            for(int i = 1 ; i < data.nbSample ; i++){
                data.timeStamp[i] = (t0 + i * sampleDeltaTime);
            }
            data.timeStart = data.timeStamp[0];
            data.timeStop = data.timeStamp[data.timeStamp.length-1];

        } catch (FileNotFoundException ex) {
            Logger.getLogger(SaveLoadFdaData.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(progress,
                        ex.getMessage() + "\n" + ex.toString(),
                        "Fda Request Failed",
                        JOptionPane.ERROR_MESSAGE);
            Logger.getLogger(SaveLoadFdaData.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (progress != null) {
                progress.hideDlg();
            }
        }

        long time1 = System.currentTimeMillis();

        System.out.println("Read in :" + (time1 - time0) + "ms");

        return data;
    }
    
    static private double findLableReadDouble(String labelToFind, String strToSearch) {
        String str = findLableReadString(labelToFind,strToSearch);
        if(str == null){
            return -1;
        }
        return Double.parseDouble(str);
    }
    
    static private String findLableReadString(String labelToFind, String strToSearch) {
        int indexStart = strToSearch.indexOf(labelToFind);
        if(indexStart == -1){
            return null;
        }
        int indexStop = strToSearch.indexOf("\r\n", indexStart);
        System.out.println(strToSearch.substring(indexStart + labelToFind.length(), indexStop));
        return strToSearch.substring(indexStart + labelToFind.length(), indexStop);
    }
    
    void calcDuration() {
        duration = (int) (timeStop - timeStart);
    }

    String getDuration() {
        switch (duration) {
            case 1: {
                return "1second";
            }
            case 2: {
                return "2seconds";
            }
            case 3: {
                return "3seconds";
            }
            case 5: {
                return "5seconds";
            }
            case 10: {
                return "10seconds";
            }
            case 60: {
                return "1minute";
            }
            case 120: {
                return "2minutes";
            }
            case 180: {
                return "3minutes";
            }
            case 240: {
                return "4minutes";
            }
            case 300: {
                return "5minutes";
            }
            case 3600: {
                return "1hour";
            }
            case 14400: {
                return "4hours";
            }
            case 28800: {
                return "8hours";
            }
            case 43200: {
                return "12hours";
            }
            case 64800: {
                return "18hours";
            }
        }
        return "durationNotMatch";
    }

}
