/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fdareader;

/**
 *
 * @author tappret
 */
public class PlanView {
    FdaPositionCorectionData data;
    FdaData global;
    final String planS;

    double[][] mean;

    double[][] min;
    double[][] max;
    double[][] stdDev;
    double[][] FFT;

    double[] averageMean;
    double[] averageStdDev;

    public PlanView(FdaData globalData,FdaPositionCorectionData data,boolean horizontalOrVertical) {
        this.global = globalData;
        this.data = data;
        
        mean = new double[global.nbSample][];
        if (global.decimateMode != FdaConstants.DecimateMode.FULL) {
            min = new double[global.nbSample][];
            max = new double[global.nbSample][];
            stdDev = new double[global.nbSample][];
        }

        averageMean = new double[data.nbIds];
        averageStdDev = new double[data.nbIds];

        if (global.decimateMode == FdaConstants.DecimateMode.FULL) {
            for (int i = 0; i < global.nbSample; ++i) {
                mean[i] = new double[data.nbIds];
            }
        } else {
            for (int i = 0; i < global.nbSample; ++i) {
                mean[i] = new double[data.nbIds];

                min[i] = new double[data.nbIds];
                max[i] = new double[data.nbIds];
                stdDev[i] = new double[data.nbIds];
            }
        }
        
        if(horizontalOrVertical){
            planS = "Horizontal";
        }else{
            planS = "Vertical";
        }
    }

    void computeAverage() {
        for (int j = 0 ; j < data.nbIds ; ++j){
            averageMean[j] = computeAverage(mean,j);
        }
        if(global.decimateMode != FdaConstants.DecimateMode.FULL){
            for (int j = 0 ; j < data.nbIds ; ++j){
                averageStdDev[j] = computeAverage(stdDev,j);
            }
        }
    }
    
    private double computeAverage(double [][] data, int index){
        double sum = 0.0;
        int tenPourcentreSample = (int)(global.nbSample * 0.1);
        for(int i = 0 ; i < tenPourcentreSample ; ++i){
            sum += data[i][index];
        }
        return sum / tenPourcentreSample;
    }
    
}
