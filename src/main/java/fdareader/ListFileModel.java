/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fdareader;

import java.io.File;
import java.io.FileFilter;
import java.util.Date;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;


/**
 *
 * @author tappret
 */
public class ListFileModel{
    
    DefaultTableModel model = new DefaultTableModel();
    File [] listFile;
    String [] listFileS;
    int nbFile;

    public ListFileModel(long start, long stop) {
        listFile = listZipFiles(MainPanel.fdaSaveLoadPath);
        initModel(model);
        
        nbFile = listFile.length;
        listFileS = new String[nbFile];
        
        filter(start, stop);
        
    }
    
    public File[] listZipFiles(String directoryName) {

        // .............list file
        File directory = new File(directoryName);

        
        return directory.listFiles(new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                return pathname.getName().toLowerCase().endsWith(".zip");
            }
        });
    } 

    static private long parseDuration(String duration) {
        switch(duration){
            case "1second":{
                return 1;
            }
            case "2seconds":{
                return 2;
            }
            case "3seconds":{
                return 3;
            }
            case "5seconds":{
                return 5;
            }
            case "10seconds":{
                return 10;
            }
            case "1minute":{
                return 60;
            }
            case "2minutes":{
                return 120;
            }
            case "3minutes":{
                return 180;
            }
            case "4minutes":{
                return 240;
            }
            case "5minutes":{
                return 300;
            }
            case "1hour":{
                return 3600;
            }
            case "4hours":{
                return 14400;
            }
            case "8hours":{
                return 28800;
            }
            case "12hours":{
                return 43200;
            }
            case "18hours":{
                return 64800;
            }
        }
        return -1;
    }

    String getFileName(int index) {
        return MainPanel.fdaSaveLoadPath +"/" + (String)(model.getValueAt(index, model.getColumnCount()-1)) + ".zip";
    }

    private void initModel(DefaultTableModel model) {
        model.addColumn("Start");
        model.addColumn("End");
        model.addColumn("Decimate Mode");
        model.addColumn("Number of Samples");
        model.addColumn("Comment");
        model.addColumn("File Name");
    }
    
    static String[] parseFileName(String fileName) throws NumberFormatException {
        return parseFileName(fileName, -1, -1);
    }
    
    static String[] parseFileName(String fileName, long start, long stop) throws NumberFormatException {
        fileName = fileName.substring(0, fileName.length()-4);
        
        String [] splited = fileName.split("_");
        long timeStart = Long.parseLong(splited[0]);
        long timeStop = timeStart + parseDuration(splited[3]);
        
        boolean noFilterDate = (start == -1 && stop == -1);
        //Check if file is in Date range

        if(!(timeStart >= start && timeStop <= stop) && !noFilterDate){
                return null;
        }
        
        String [] row = new String[6];
        row[0] = MainPanel.SDF.format(new Date(timeStart));
        row[1] = MainPanel.SDF.format(new Date(timeStop));
        row[2] = splited[2];
        row[3] = splited[3];
        if(splited.length >= 5){
            row[4] = splited[4];
        }else{
            row[4] = "";
        }
        row[5] = fileName;
        
        return row;
    }

    TableModel filter(long start, long stop) {
        model = new DefaultTableModel();
        initModel(model);
        
        for (int i = 0 ; i < nbFile ; i++) {
            try{
                String[] row = parseFileName(listFile[i].getName(),start,stop);
                listFileS[i] = "";
                if(row != null){
                    model.addRow(row);
                }
            }catch(NumberFormatException ex){
                //skip file
            }
        }
        return model;
    }

    TableModel filter(String str) {
        model = new DefaultTableModel();
        initModel(model);
        
        for (int i = 0 ; i < nbFile ; i++) {
            String[] row;
            if(listFileS[i].contains(str)){
                try{
                    row = parseFileName(listFile[i].getName());
                    listFileS[i] = "";
                    for (String row1 : row) {
                        listFileS[i] += row1 +"_" ;
                    }
                    model.addRow(row);
                }catch(NumberFormatException ex){
                    //skip file
                }
            }
        }
        
        return model;
    }
    
}
