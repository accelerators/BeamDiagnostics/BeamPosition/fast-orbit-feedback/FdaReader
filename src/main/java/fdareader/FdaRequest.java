/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fdareader;

import fdareader.FdaConstants.DecimateMode;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.SocketChannel;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.IntBuffer;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author tappret
 */
public class FdaRequest {
    private final String hostname;
    private final int port;
    
    DecimalFormat df = new DecimalFormat("#.000");
        

    public FdaRequest(String hostname, int port ) throws IOException {
        this.hostname = hostname;
        this.port = port;
        df.setMaximumFractionDigits(8);
    }
    
    String CommandRequest(String cmd) throws IOException {
        SocketChannel sockChan;
        sockChan = SocketChannel.open();
        sockChan.socket().setTcpNoDelay(true);
        InetSocketAddress addr = new InetSocketAddress(hostname, port);
        sockChan.connect(addr);
        
        ByteBuffer buffCmdRequest = ByteBuffer.allocate(1024);
        buffCmdRequest.clear();
        buffCmdRequest.put(cmd.getBytes());

        buffCmdRequest.flip();
        
        while(buffCmdRequest.hasRemaining()) {
            int writeByte = sockChan.write(buffCmdRequest);
            //System.out.println(writeByte);
        }
        buffCmdRequest.clear();
        
        int nbByteRead = sockChan.read(buffCmdRequest);
        byte [] minibuff = new byte[nbByteRead];
        System.arraycopy(buffCmdRequest.array(), 0, minibuff, 0, nbByteRead);
        
        
        sockChan.finishConnect();
        sockChan.close();
               
        return new String(minibuff);
    }
    
    
    FdaPositionCorectionData request(FdaData globalData,String ids,int nbIdsRequested, ThreadDlg loadingPanel, long nbByteAttend , int nbByteAlreadyRead) throws IOException, FdaException{
        return request(globalData, ids, nbIdsRequested, loadingPanel, nbByteAttend,nbByteAlreadyRead,false,1.0);
    }
    
    FdaPositionCorectionData request(FdaData globalData,String ids,int nbIdsRequested, ThreadDlg loadingPanel, long nbByteAttend, int nbByteAlreadyRead, boolean readWithTheSetOfTimeStamp, double factor) throws IOException, FdaException{
        InetSocketAddress addr = new InetSocketAddress(hostname, port);
        
        if(nbByteAttend > FdaConstants.MAX_SIZE_REQUESTABLE){
            throw new FdaException("asked too much data");
        }
        if(nbByteAttend < 2){
            throw new FdaException("asked not few data");
        }
        
        boolean forceRequest = false;
        ByteBuffer buffIn = ByteBuffer.allocate(1024);
        ByteBuffer buffOut = ByteBuffer.allocate((int)nbByteAttend);
        
        String cmd = this.buildCommand(ids, globalData,forceRequest);
        //System.out.println(cmd);
        
        
        //read Socket
        SocketChannel sockChan;
        sockChan = SocketChannel.open();
        sockChan.socket().setTcpNoDelay(true);
        sockChan.connect(addr);
        
        buffIn.put(cmd.getBytes());

        buffIn.flip();
        
        while(buffIn.hasRemaining()) {
            sockChan.write(buffIn);
            //int writeByte = sockChan.write(buffIn);
            //System.out.println(writeByte);
        }
        
        int nbbyte;
        int nbByteReadForAlloc = 0;
        while((nbbyte = sockChan.read(buffOut)) > 0){
            /*if(nbbyte == 0){
                break;
            }*/
            nbByteReadForAlloc += nbbyte;
            if(loadingPanel != null){
                loadingPanel.setProgress(((double)(nbByteReadForAlloc + nbByteAlreadyRead) / nbByteAttend), null);
            }
        }
        
        //System.out.println("ByteRead:" + nbByteRead);
        if(buffOut.get(0) != 0){
            byte [] buffText = new byte[nbByteReadForAlloc];
            buffOut.flip();
            buffOut.get(buffText, 0, nbByteReadForAlloc);
            System.err.println(cmd);
            throw new FdaException(new String(buffText) + "StartDate:" + MainPanel.SDF_MILLI.format(new Date((long) globalData.timeStart * 1000)) + "\nStopDate:" + MainPanel.SDF_MILLI.format(new Date((long) globalData.timeStop* 1000)) );
        }
        buffOut = (ByteBuffer)buffOut.position(1);
        buffOut.order(ByteOrder.LITTLE_ENDIAN);
        
        int nbInt = (nbByteReadForAlloc-1)/4;
        IntBuffer intBuff = buffOut.asIntBuffer();
                
        
        sockChan.finishConnect();
        sockChan.close();
        
        FdaPositionCorectionData fdaData;
        fdaData = parseBuffer(globalData,intBuff,nbInt, nbIdsRequested,readWithTheSetOfTimeStamp,factor);
        globalData.calcDuration();
        
        return fdaData;
    }
    
    private FdaPositionCorectionData parseBuffer(FdaData globalData,IntBuffer buff,int nbInt, int nbIdsRequested,boolean readWithTheSetOfTimeStamp,double factor) {
        //System.out.println("Sizebuff:"+ buff.length);
        int indexStartData = 0;
        
        long initialTimeStamp = -1;
        
        boolean askSampleCount = false;
        if(globalData.timeStampMode != FdaConstants.TimeStampMode.TIMESTAMP){
            askSampleCount = true;
        }
        
        int numberOfSample;
        if(askSampleCount){
            numberOfSample = (int)FdaConstants.intToLong(buff.get(indexStartData+1), buff.get(indexStartData));
            indexStartData += 2;
        }else if(globalData.dataTypeReq != FdaConstants.DataTypeRequest.FOUR_VALUE || globalData.decimateMode == DecimateMode.FULL){
            numberOfSample = (int) ((nbInt - indexStartData) / nbIdsRequested / 2);
        }else{
            numberOfSample = (int) ((nbInt - indexStartData) / nbIdsRequested / 2 / 4);
        }
        
        
        //force nbsample at the nbsample of the worst request
        if(globalData.nbSample > numberOfSample){
            readWithTheSetOfTimeStamp = true;
            globalData.nbSample = numberOfSample;
        }
        if(numberOfSample > globalData.nbSample && globalData.nbSample == 0){
            readWithTheSetOfTimeStamp = true;
            globalData.nbSample = numberOfSample;
        }
        /*
        if(globalData.nbSample != 0){
            if(globalData.nbSample > numberOfSample){
                globalData.nbSample = numberOfSample;
            }
        }
        globalData.nbSample = numberOfSample;*/
        
        if(readWithTheSetOfTimeStamp){
            globalData.timeStamp = new double[globalData.nbSample];
        }
        
        
        
        //System.out.println("numberOfIndexTime:"+(numberOfIndexTime-2));
        //System.out.println("SampleCount:"+SampleCount);

        switch (globalData.timeStampMode){
            case TIMESTAMP:
                initialTimeStamp = FdaConstants.intToLong(buff.get(indexStartData+1), buff.get(indexStartData)) / 1000;
                indexStartData += 2;
                break;
            case EXTENDED_TIMESTAMP_TE:
                initialTimeStamp = FdaConstants.intToLong(buff.get(indexStartData+1), buff.get(indexStartData)) / 1000;
                indexStartData += 2;
                break;
        }
        
        FdaPositionCorectionData data = new FdaPositionCorectionData(globalData,nbIdsRequested);
        //set initial TimeStamp
        //data.timeStamp[numberOfIndexTime-1] = initialTimeStamp;
        //data.timeStamp[0] = initialTimeStamp;
        //System.out.println("initialTimeStamp:" + new Date(initialTimeStamp));
        double coefTime = FdaConstants.getSampleDeltaTime(globalData.decimateMode);
        
        long cpt = 0;

        if(globalData.timeStampMode == FdaConstants.TimeStampMode.EXTENDED_TIMESTAMP_TE){
            if(readWithTheSetOfTimeStamp){
                for(int i = 0 ; i < globalData.nbSample ; ++i ){
                    globalData.timeStamp[i] = FdaConstants.intToLong(buff.get(indexStartData+1), buff.get(indexStartData)) / 1000;
                }
            }
        }else{
            buff.position(indexStartData);
            
            double correctionFactor = 1;
            if(nbIdsRequested == FdaConstants.NB_CORRESTOR){
                correctionFactor = FdaConstants.CONVERTION_FACTOR_FOR_CORRECTOR;
            }
            if(factor != 1.0){
                correctionFactor = factor;
            }
            
            for(int i = 0 ; i < globalData.nbSample ; ++i ){
                
                //with DD desimate factor is 16384 then 1 point every 1.6 seconds
                //int idx = numberOfIndexTime - 1 - i;
                
                if(readWithTheSetOfTimeStamp){
                    globalData.timeStamp[i] = (initialTimeStamp + i * coefTime);
                }

                //System.out.println(" sample:" + (i+1) +"\tindex:"+i +"\ttimeStamp:" + (initialTimeStamp + (numberOfIndexTime-i) * 1600));
                
                for(int j = 0 ; j < nbIdsRequested ; ++j ){
                    cpt++;
                    //System.out.println("\tcpt:" + cpt);
                    try{
                        //if((j+1) == 1)
                        //System.out.println("id:" + (j+1) + " sample:" + (i+1) +"\tindex:"+((i * numberOfIds *2) + (j * 2) + indexStartData )+"\tdata:" + buff[ (i * numberOfIds * 2) + (j * 2) + indexStartData ] );
                        if(globalData.decimateMode == DecimateMode.FULL){
                            data.horizontal.mean[i][j] = buff.get()*correctionFactor;
                            data.vertical.mean[i][j]   = buff.get()*correctionFactor;
                        }else{
                            data.horizontal.mean[i][j]  = buff.get()*correctionFactor;
                            data.vertical.mean[i][j]    = buff.get()*correctionFactor;
                            data.horizontal.min[i][j]   = buff.get()*correctionFactor;
                            data.vertical.min[i][j]     = buff.get()*correctionFactor;
                            data.horizontal.max[i][j]   = buff.get()*correctionFactor;
                            data.vertical.max[i][j]     = buff.get()*correctionFactor;
                            data.horizontal.stdDev[i][j]= buff.get()*correctionFactor;
                            data.vertical.stdDev[i][j]  = buff.get()*correctionFactor;
                        }
                    } catch (Exception ex){
                        if(globalData.decimateMode == DecimateMode.FULL){
                            System.err.println("i:"+i+" j:"+j+" indexBuffer:"+ ( (i * nbIdsRequested * 2) + (j * 2) + indexStartData) );
                            System.err.println("cpt:"+ cpt);
                        }else{
                            System.err.println("i:"+i+" j:"+j+" indexBuffer:"+ ( (i * nbIdsRequested * 2 * 4 ) + (j * 2 * 4) + indexStartData) );
                            System.err.println("cpt:"+ cpt);
                        }
                        Logger.getLogger(MainPanel.class.getName()).log(Level.SEVERE, null, ex);
                    }

                }
            }
        }
        data.computeAverage();
        return data;
    }

    //String ids,int nbIdsRequested, double timeStart, double timeStop,DecimateMode decimateFactor,DataTypeRequest dataTypeRequest,TimeStampMode timeStampMode , ThreadDlg loadingPanel, long nbByteAttend , int nbByteAlready
    private String buildCommand(String ids,FdaData globalData,boolean forceRequest){
        boolean askSampleCount = false;
        if(globalData.timeStampMode != FdaConstants.TimeStampMode.TIMESTAMP){
            askSampleCount = true;
        }
            
        
        //build request
        String cmd = "R";
        switch(globalData.decimateMode){
            case FULL:
                cmd += "F";
                break;
            case DECIMATE64:
                cmd += "D";
                break;
            case DECIMATE256:
                cmd += "DD";
                break;
        }
        
        if(globalData.decimateMode != DecimateMode.FULL && globalData.dataTypeReq != FdaConstants.DataTypeRequest.FOUR_VALUE){
            cmd += "F"+ globalData.dataTypeReq.getValue();
        }
        //System.out.println(df.format(timeStop));
        
        cmd += "M"+ ids + "S" + df.format(globalData.timeStart) +"ES"+ df.format(globalData.timeStop);
        
        if(askSampleCount){
            cmd += "N";
        }
        if(forceRequest){
            cmd += "A";
        }
        switch(globalData.timeStampMode){
            case TIMESTAMP:
                cmd += "T";
                break;
                
            case EXTENDED_TIMESTAMP_TE:
                cmd += "TE";
                break;
        
            case EXTENDED_TIMESTAMP_TA:
                cmd += "TA";
                break;
        }
        
        return cmd + FdaConstants.END_FRAME;
    }

}
