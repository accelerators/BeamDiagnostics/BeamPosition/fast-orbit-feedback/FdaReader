/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fdareader;

import fr.esrf.tangoatk.widget.attribute.I3DTrendChangeListener;
import fr.esrf.tangoatk.widget.attribute.I3DTrendCursorListener;
import fr.esrf.tangoatk.widget.attribute.NumberSpectrumTrend3DViewer;
import fr.esrf.tangoatk.widget.attribute.NumberSpectrumTrend3DViewerListener;
import fr.esrf.tangoatk.widget.util.chart.IJLChartListener;
import fr.esrf.tangoatk.widget.util.chart.JLAxis;
import fr.esrf.tangoatk.widget.util.chart.JLChartEvent;
import fr.esrf.tangoatk.widget.util.chart.JLDataView;
import java.awt.Point;
import java.text.DecimalFormat;
import java.util.Date;

/**
 *
 * @author tappret
 */
public class Image2Trend implements IJLChartListener, I3DTrendCursorListener, I3DTrendChangeListener, NumberSpectrumTrend3DViewerListener {

    private final NumberSpectrumTrend3DViewer image;
    private final NumberSpectrumTrend3DViewer otherImage;
    private final MyNumberSepctrumViewer trendSrView;
    private final MyNumberSepctrumViewer trendTimeView;
    private final MainSplitPanel parent;
    private final boolean positionOrCorrector;
    private final boolean horizontalOrVertical;
    private final DecimalFormat df = new DecimalFormat("###.##");
    private FdaData global;
    private Date lastClickedTimeView = null;
    private FdaPositionCorectionData viewData;

    public Image2Trend(NumberSpectrumTrend3DViewer image, NumberSpectrumTrend3DViewer otherImage, MainSplitPanel parent, MyNumberSepctrumViewer trendSrView, MyNumberSepctrumViewer trendTimeView, boolean positionOrCorrector, boolean horizontalOrVertical) {
        this.image = image;
        this.otherImage = otherImage;
        this.trendSrView = trendSrView;
        this.parent = parent;
        this.trendTimeView = trendTimeView;
        this.positionOrCorrector = positionOrCorrector;
        this.horizontalOrVertical = horizontalOrVertical;
        
        if (!positionOrCorrector) {
            image.setVerticalZoom(2);
        }
        image.setHorizontalZoom(-2);

        image.getXAxis().setAnnotation(JLAxis.TIME_ANNO);

        image.addCursorListener((I3DTrendCursorListener) this);
        image.addChangeListener((I3DTrendChangeListener) this);
        image.getTrend().updateXRange(false);

        image.addNumberSpectrumTrend3DViewerListener((NumberSpectrumTrend3DViewerListener) this);

        setLabelOnSrView();

        trendTimeView.getXAxis().setAnnotation(JLAxis.TIME_ANNO);

        trendTimeView.setJLChartListener((IJLChartListener) this);

        trendSrView.setJLChartListener((IJLChartListener) this);

        //trendTimeView.addOwnDataViewCurrent();
    }

    @Override
    public String[] clickOnChart(JLChartEvent jlce) {
        double x = jlce.getXValue();
        double y = jlce.getYValue();
        boolean current = false;

        String[] str = new String[2];
        if (jlce.getSource() == trendSrView) {
            int idx = jlce.getDataViewIndex();
            if (positionOrCorrector) {
                str[0] = "Bpm: " + FdaConstants.LIST_IDS_BPM_TITLE[(int) idx];
            } else {
                str[0] = "Corrector: " + FdaConstants.LIST_IDS_CORRECTOR_TITLE[(int) idx];
            }
        } else {
            if (global.fft) {
                str[0] = "Freqency: " + x + "Hz";
            } else {
                lastClickedTimeView = new Date((long) x);
                double usec = (x - (double)((long)x)) * 1000;
                str[0] = MainPanel.SDF_MILLI.format(lastClickedTimeView) + String.format("%03d%n", (int)usec);
                //str[0] = Double.toString(Math.floor(x) - x);
            }

            if (jlce.getDataView() == trendTimeView.getDataViewCurrent()) {
                current = true;
            }
        }

        String unit;
        String header;
        DecimalFormat df = null;
        if (global.fft) {
            unit = "nm";
            header = "Value";
        }else if (current) {
            unit = "mA";
            header = "Current";
            df = new DecimalFormat("0.##");
        } else if (this.positionOrCorrector) {
            unit = "nm";
            header = "Position";
        } else {
            header = "Corretion";
            unit = "mA";
            df = new DecimalFormat("0.###");
        }
        if(df != null){
            str[1] = header +"=" + df.format(y) + " " + unit;
        } else{
            str[1] = header +"=" + (long)y + " " + unit;
        }
        return str;
    }

    @Override
    public void cursorMove(NumberSpectrumTrend3DViewer src, int x, int y) {
        Point p = new Point(x, y);
        //System.out.println("x:"+x + " y:" + y);
        if (checkPointAvailable(p)) {
            parent.refrechTrend(p);
            parent.parent.refrechTrend(parent, new Point(p));
        }
    }

    private boolean checkPointAvailable(Point p) {
        return p.x < global.nbSample && p.x >= 0 && p.y < viewData.nbIds && p.y >= 0;
    }

    @Override
    public void zoomChanged(NumberSpectrumTrend3DViewer src, int hZoom, int vZoom) {
        if (src == image) {
            otherImage.setHorizontalZoom(hZoom);
            otherImage.setVerticalZoom(vZoom);
            otherImage.commit();
        } else if (src == otherImage) {
            image.setHorizontalZoom(hZoom);
            image.setVerticalZoom(vZoom);
            image.commit();
        }
        Point p = parent.getLastPoint();
        src.setCursorPos(p.x, p.y);
    }

    @Override
    public void horinzontalScrollChanged(NumberSpectrumTrend3DViewer src, int value) {
        if (src == image) {
            otherImage.setHorinzontalScrollPos(value);
        } else if (src == otherImage) {
            image.setHorinzontalScrollPos(value);
        }
    }

    @Override
    public void verticalScrollChanged(NumberSpectrumTrend3DViewer src, int value) {
        if (src == image) {
            otherImage.setVertitalScrollPos(value);
        } else if (src == otherImage) {
            image.setVertitalScrollPos(value);
        }
    }

    private void setData3D_Viewer() {

        int nbLigne = global.timeStamp.length;

        image.getXAxis().setAutoScale(false);

        //set Renderer Details
        String xOrZ;
        if (horizontalOrVertical) {
            xOrZ = "X";
        } else {
            xOrZ = "Z";
        }

        String positionOrCorrectorS;
        String bpmOrCorrector;
        String[] listIds;
        if (positionOrCorrector) {
            positionOrCorrectorS = "Position";
            bpmOrCorrector = "Bpm";
            listIds = FdaConstants.LIST_IDS_BPM_TITLE;
        } else {
            positionOrCorrectorS = "Correction";
            bpmOrCorrector = "Corrector";
            listIds = FdaConstants.LIST_IDS_CORRECTOR_TITLE;
        }

        image.setName(xOrZ + " " + positionOrCorrectorS);
        image.setYName(bpmOrCorrector);
        image.setYIndexName(listIds);
        if (positionOrCorrector) {
            image.setUnit("nm");
        } else {
            image.setUnit("mA");
        }
        image.setValueName("value");

        if (!global.fft) {
            image.getXAxis().setMinimum(global.timeStamp[nbLigne - 1]);
            image.getXAxis().setMaximum(global.timeStamp[0]);
            image.getXAxis().setAnnotation(JLAxis.TIME_ANNO);
        } else {
            image.getXAxis().setMinimum(0);
            image.getXAxis().setMaximum(FdaConstants.NB_SAMPLE_FFT);
        }

        //prepare Data
        double[][] dataValue;
        double[] averageDataValue;
        double[] timeStampOrFrequencies = global.timeStamp;

        PlanView plan = viewData.getPlan(horizontalOrVertical);

        if (global.meanOrStdDev) {
            dataValue = plan.mean;
            averageDataValue = plan.averageMean;
        } else {
            dataValue = plan.stdDev;
            averageDataValue = plan.averageStdDev;
        }

        if (global.fft) {
            double[][] dataSrc = dataValue;
            int nbSampleForFFT = viewData.getNbPointForDoFFTTo(FdaConstants.NB_SAMPLE_FFT);
            dataValue = new double[nbSampleForFFT][];
            timeStampOrFrequencies = new double[nbSampleForFFT];
            for (int i = 0; i < dataValue.length; ++i) {
                dataValue[i] = new double[viewData.nbIds];
            }

            try {
                for (int i = 0; i < viewData.nbIds; ++i) {
                    viewData.doFFT(dataSrc, i, global.ac);
                    double[] modulus = viewData.getModulus();
                    for (int j = 0; j < modulus.length; ++j) {
                        dataValue[j][i] = modulus[j];
                    }
                }
                double[] frequencies = viewData.getFrequencies();
                for (int i = 0; i < timeStampOrFrequencies.length; ++i) {
                    timeStampOrFrequencies[i] = (long) frequencies[i];
                }
                global.frequencies = timeStampOrFrequencies;
                plan.FFT = dataValue;
            } catch (FdaException ex) {
                ex.printStackTrace();
            }
        }

        if (global.ac && !global.fft) {
            double[][] dataSrc = dataValue;
            dataValue = new double[dataSrc.length][];

            for (int i = 0; i < dataSrc.length; ++i) {
                dataValue[i] = new double[dataSrc[i].length];
                for (int j = 0; j < dataSrc[i].length; ++j) {
                    dataValue[i][j] = dataSrc[i][j] - averageDataValue[j];
                }
            }
        }
//        
//        long [] timeStampOrFrequenciesLong = new long[timeStampOrFrequencies.length];
//        for (int i = 0; i < dataValue.length; ++i) {
//            timeStampOrFrequenciesLong[i] = (long)timeStampOrFrequencies[i];
//        }

        image.setData(timeStampOrFrequencies, dataValue);

        image.repaint();
    }

    @Override
    public String getStatusLabel(NumberSpectrumTrend3DViewer src, int x, int y, long index, double value) {
        String status;

        //set Renderer Details
        String xOrZ;
        if (horizontalOrVertical) {
            xOrZ = "X";
        } else {
            xOrZ = "Z";
        }

        String positionOrCorrectorS;
        String bpmOrCorrector;
        String[] listIds;
        if (this.positionOrCorrector) {
            positionOrCorrectorS = "Position";
            bpmOrCorrector = "Bpm";
            listIds = FdaConstants.LIST_IDS_BPM_TITLE;
        } else {
            positionOrCorrectorS = "Correction";
            bpmOrCorrector = "Corrector";
            listIds = FdaConstants.LIST_IDS_CORRECTOR_TITLE;
        }

        if (x == -1 || y == -1) {
            return (" " + xOrZ + " " + positionOrCorrectorS);
        }

        String unit;
        if (this.positionOrCorrector) {
            unit = "nm";
        } else {
            unit = "mA";
        }

        if (!global.fft) {
            double usec = (global.timeStamp[global.timeStamp.length - x -1] - index) * 1000;
            String usecIfFullData = "";
            if(global.decimateMode == FdaConstants.DecimateMode.FULL){
                usecIfFullData = String.format("%03d%n", (int)usec);
            }
            status = " " + xOrZ + " " + positionOrCorrectorS + " | " + MainPanel.SDF_MILLI.format(new Date((long)global.timeStamp[global.nbSample - x - 1])) + usecIfFullData +" value=" + df.format(value) + " " + unit + " at " + bpmOrCorrector + "=" + listIds[y];
        } else {
            status = " " + xOrZ + " " + positionOrCorrectorS + " | " + df.format(global.frequenciesFFT[global.frequenciesFFT.length - 1 - x]) + " Hz value=" + df.format(value) + " " + unit + " at " + bpmOrCorrector + "=" + listIds[y];
        }
        return status;
    }

    void refrechTrend(Point point) {
        refrechTrend(trendSrView, point);
        refrechTrend(trendTimeView, point);
    }

    private void refrechTrend(MyNumberSepctrumViewer trend, Point point) {
        int index = point.x;
        int id = point.y;
        int nbSample;
        if (!global.fft) {
            nbSample = global.nbSample;
        } else {
            nbSample = global.frequenciesFFT.length;
        }

        //to delete on
        if (index < 0 || index > nbSample - 1) {
            return;
        }
        if (id > viewData.nbIds - 1 || id < 0) {
            return;
        }

        //reverse index
        index = nbSample - 1 - index;

        PlanView plan = viewData.getPlan(horizontalOrVertical);

        trend.getDataView().reset();
        JLDataView dataViewMin = trend.getDataViewMin();
        JLDataView dataViewMax = trend.getDataViewMax();
        trend.getDataViewCurrent().reset();
        dataViewMin.reset();
        dataViewMax.reset();

        if (trend == trendSrView) {
            refrechTrendSrView(plan, index, id);
        } else {
            refrechTrendTimeView(plan, index, id);
        }

        if (global.decimateMode != FdaConstants.DecimateMode.FULL
                && global.meanOrStdDev) {
            if (global.max) {
                trend.addOwnDataViewMax();
            } else {
                trend.removeOwnDataViewMax();
            }

            if (global.min) {
                trend.addOwnDataViewMin();
            } else {
                trend.removeOwnDataViewMin();
            }
        } else {
            trend.removeOwnDataView();
        }

        trend.getDataView().commitChange();
        trend.repaint();
    }

    private void refrechTrendSrView(PlanView plan, int index, int id) {
        /*String planS;
        if(plan == data.horizontal){
            planS =  "Horizontal";
        }else{
            planS =  "Hertical";
        }
        System.out.println(planS + " index:" + index +" id:"+ id);*/

        double coefData;
        if (positionOrCorrector) {
            coefData = FdaConstants.NB_BPM_BY_CELL;
        } else {
            coefData = FdaConstants.NB_CORRESTOR_BY_CELL;
        }

        double[][] mainDataToDraw;
        double[][] dataToShowMin;
        double[][] dataToShowMax;
        double[] averageToSoustract;

        if (!global.fft) {
            if (global.meanOrStdDev) {
                mainDataToDraw = plan.mean;
                averageToSoustract = plan.averageMean;
            } else {
                mainDataToDraw = plan.stdDev;
                averageToSoustract = plan.averageStdDev;
            }
        } else {
            mainDataToDraw = plan.FFT;
            averageToSoustract = plan.averageMean;
        }
        if(mainDataToDraw == null){
            return;
        }
        
        dataToShowMin = plan.min;
        dataToShowMax = plan.max;

        JLDataView dataViewMin = trendSrView.getDataViewMin();
        JLDataView dataViewMax = trendSrView.getDataViewMax();

        if (!global.ac || global.fft) {
            for (int i = 0; i < viewData.nbIds; ++i) {
                trendSrView.getDataView().add(i / coefData, mainDataToDraw[index][i], false);
                if (global.decimateMode != FdaConstants.DecimateMode.FULL) {
                    dataViewMin.add(i / coefData, dataToShowMin[index][i], false);
                    dataViewMax.add(i / coefData, dataToShowMax[index][i], false);
                }
            }
        } else {
            for (int i = 0; i < viewData.nbIds; ++i) {
                trendSrView.getDataView().add(i / coefData, mainDataToDraw[index][i] - averageToSoustract[i], false);
                if (global.decimateMode != FdaConstants.DecimateMode.FULL) {
                    dataViewMin.add(i / coefData, dataToShowMin[index][i] - averageToSoustract[i], false);
                    dataViewMax.add(i / coefData, dataToShowMax[index][i] - averageToSoustract[i], false);
                }
            }
        }

        if (!global.fft) {
            trendSrView.getDataView().setName(plan.planS + " View of StorageRing at " + MainPanel.SDF_MILLI.format(new Date((long)this.global.timeStamp[index])));
        } else {
            trendSrView.getDataView().setName(plan.planS + " View in Frequency at " + this.global.frequencies[index] + " Hz");
        }
    }

    public void printPoint(Point point) {
        String bpmOrCorrector;
        String[] listText;
        if (parent == parent.parent.mspBpm) {
            listText = FdaConstants.LIST_IDS_BPM_TITLE;
            bpmOrCorrector = "BPM ";
        } else {
            listText = FdaConstants.LIST_IDS_CORRECTOR_TITLE;
            bpmOrCorrector = "COR ";
        }
        String planS;
        if (this.horizontalOrVertical) {
            planS = "Horizont";
        } else {
            planS = "Vertical";
        }
        System.out.println(bpmOrCorrector + planS + " index:" + point.x + " id:" + listText[point.y]);
    }

    private void refrechTrendTimeView(PlanView plan, int index, int id) {
        //printPoint(new Point(index,id));

        int nbSample;
        if (!global.fft) {
            nbSample = global.nbSample;
        } else {
            nbSample = global.frequenciesFFT.length;
        }

        double[][] mainDataToDraw;
        double[][] dataToShowMin;
        double[][] dataToShowMax;
        double[] averageToSoustract;

        if (!global.fft) {
            if (global.meanOrStdDev) {
                mainDataToDraw = plan.mean;
                averageToSoustract = plan.averageMean;
            } else {
                mainDataToDraw = plan.stdDev;
                averageToSoustract = plan.averageStdDev;
            }
        } else {
            mainDataToDraw = plan.FFT;
            averageToSoustract = plan.averageMean;
        }
        dataToShowMin = plan.min;
        dataToShowMax = plan.max;

        JLDataView dataViewMin = trendTimeView.getDataViewMin();
        JLDataView dataViewMax = trendTimeView.getDataViewMax();

        JLDataView dataViewCurrent = trendTimeView.getDataViewCurrent();
        
        if (global.current != null) {
            for (int i = 0; i < nbSample; ++i) {
                //double z = global.current.vertical.mean[i][0] / 1000000;
                //double current = 92.58 *  (z + 15) /  (15 - z);
                //current = (current-105.0423) * 6500;
                double current = global.current.horizontal.mean[i][0];
                current = current * parent.parent.CurrentFactorDouble;
                dataViewCurrent.add(this.global.timeStamp[i], current);
            }
        } else {
            global.viewCurrent = false;
        }

        if (global.viewCurrent) {
            trendTimeView.addOwnDataViewCurrent();
        } else {
            trendTimeView.removeOwnDataViewCurrent();
        }

        if (!global.fft) {
            trendTimeView.getXAxis().setAnnotation(JLAxis.TIME_ANNO);
            trendTimeView.getXAxis().setAutoScale(true);
        } else {
            trendTimeView.getDataView().setSamplingFrequency(FdaConstants.SAMPLE_FREQUENCY);
            trendTimeView.getXAxis().setAnnotation(JLAxis.VALUE_ANNO);
            trendTimeView.getXAxis().setAutoScale(false);
            trendTimeView.getXAxis().setMinimum(0);
            trendTimeView.getXAxis().setMaximum(FdaConstants.NB_SAMPLE_FFT);
        }

        if (global.fft) {
            if(plan.FFT == null){
                return;
            }
            for (int i = 0; i < nbSample; ++i) {
                trendTimeView.getDataView().add(this.global.frequencies[i], plan.FFT[i][id], false);
            }
        } else if (!global.ac) {
            for (int i = 0; i < nbSample; ++i) {
                trendTimeView.getDataView().add(this.global.timeStamp[i], mainDataToDraw[i][id], false);
                if (global.decimateMode != FdaConstants.DecimateMode.FULL) {
                    dataViewMin.add(this.global.timeStamp[i], dataToShowMin[i][id]);
                    dataViewMax.add(this.global.timeStamp[i], dataToShowMax[i][id]);
                }
            }
        } else {
            for (int i = 0; i < nbSample; ++i) {
                trendTimeView.getDataView().add(this.global.timeStamp[i], mainDataToDraw[i][id] - averageToSoustract[id], false);
                if (global.decimateMode != FdaConstants.DecimateMode.FULL) {
                    dataViewMin.add(this.global.timeStamp[i], dataToShowMin[i][id] - averageToSoustract[id]);
                    dataViewMax.add(this.global.timeStamp[i], dataToShowMax[i][id] - averageToSoustract[id]);
                }
            }
        }

        String bpmCorector;
        String[] listString;
        if (positionOrCorrector) {
            bpmCorector = "Bpm";
            listString = FdaConstants.LIST_IDS_BPM_TITLE;
        } else {
            bpmCorector = "Corrector";
            listString = FdaConstants.LIST_IDS_CORRECTOR_TITLE;
        }

        String subString;
        if (!global.fft) {
            subString = plan.planS + " View of " + bpmCorector;
        } else {
            subString = plan.planS + " FFT of " + bpmCorector;
        }

        trendTimeView.getDataView().setName(subString + " " + listString[id]);
    }

    private void setLabelOnSrView() {
        trendSrView.getXAxis().setAutoScale(false);
        trendSrView.getXAxis().setMinimum(0);
        trendSrView.getXAxis().setMaximum(31.86);
        trendSrView.getXAxis().setLabels(FdaConstants.LIST_CELL, FdaConstants.LIST_CORRECTOR_INDEX);
    }

    void setData(FdaData data, Point lastPoint) {
        this.global = data;
        viewData = getViewData();

        Thread t = new Thread(new Runnable() {

            @Override
            public void run() {
                synchronized (image) {
                    setData3D_Viewer();
                    refrechTrend(parent.getLastPoint());
                }
            }
        });
        t.start();
    }

    void clearData() {
        image.clearData();

        trendSrView.resetDataViews();
        setLabelOnSrView();
        trendTimeView.resetDataViews();

        trendSrView.repaint();
        trendTimeView.repaint();
    }

    Date getLastClickedTimeView() {
        return lastClickedTimeView;
    }

    void setCursors(Point p) {
        /*otherImage.setCursorPos(p.x, p.y);*/
        image.setCursorPos(p.x, p.y);
    }

    private FdaPositionCorectionData getViewData() {
        if (positionOrCorrector) {
            return global.position;
        } else {
            return global.correction;
        }
    }
}
