/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fdareader;

import com.jogamp.common.util.ReflectionUtil;
import com.toedter.calendar.JCalendar;
import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.DeviceAttribute;
import fr.esrf.TangoApi.DeviceProxy;
import fr.esrf.tangoatk.core.AttributeList;
import fr.esrf.tangoatk.core.ConnectionException;
import fr.esrf.tangoatk.core.INumberScalar;
import fr.esrf.tangoatk.core.IStringScalar;
import fr.esrf.tangoatk.widget.util.ATKDiagnostic;
import fr.esrf.tangoatk.widget.util.ATKGraphicsUtils;
import fr.esrf.tangoatk.widget.util.ErrorHistory;
import fr.esrf.tangoatk.widget.util.ErrorPane;
import fr.esrf.tangoatk.widget.util.JSmoothProgressBar;
import fr.esrf.tangoatk.widget.util.Splash;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Event;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.ConnectException;
import java.net.InetAddress;
import java.net.URI;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;

/**
 *
 * @author tappret
 */
public class MainPanel extends javax.swing.JFrame {
    private final static String VERSION;
    static // find the version number in the property file
    {
        java.util.Properties props = new java.util.Properties();
        System.out.println(MainPanel.class.getClassLoader().getResource("fdareader.properties"));
        java.io.InputStream stream = MainPanel.class.getClassLoader().getResourceAsStream("fdareader.properties");
        String propValue = null;
        try
        {
            props.load(stream);
            propValue = props.getProperty("fdareader.version");
//            System.out.println(propValue);
        }
        catch (java.io.IOException ex) {}

        if (propValue == null)
            VERSION = "xx";
        else
            VERSION = propValue;
    }
    
    private final static String DEVICE_FDA_STATUS = "srdiag/bpm/archiver-status";
    private final static String ATTRIBUTE_FDA_LAST_DATA_AVAILABLE = "LastDataAvailable";
    private final static String ATTRIBUTE_FDA_EARLIEST_DATA_AVAILABLE = "EarliestDataAvailable";
    private final static String ATTRIBUTE_FDA_LAST_DATA_AVAILABLE_EPOCH = "LastDataAvailableEpoch";
    private final static String ATTRIBUTE_FDA_EARLIEST_DATA_AVAILABLE_EPOCH = "EarliestDataAvailableEpoch";
    private final static String APPLI_NAME = "FdaReader";
    
    private boolean startup = true;
    private boolean threadStop = false;
    private final Thread refreshAvailableDate;
    //public final static String PATH_OF_SAVE = "/tmp_14_days/tappret/FdaDataSave/";
    public static String fdaSaveLoadPath;
    public double CurrentFactorDouble;
    public final static String DATE_FORMAT = "dd/MM/yyyy HH:mm:ss";
    public final static String DATE_FORMAT_MILLI = "dd/MM/yyyy HH:mm:ss.SSS";

    public final static SimpleDateFormat SDF = new SimpleDateFormat(DATE_FORMAT);
    public final static SimpleDateFormat SDF_MILLI = new SimpleDateFormat(DATE_FORMAT_MILLI);

    private static final ErrorHistory ERROR_HISTORY_FRAME = new ErrorHistory();

    private double lastDataAvailable;
    private double earliestDataAvailable;
    private ThreadDlg loadingPanel;
    ComboPreSelection comboPreSelet;

    AttributeList attl = new AttributeList();
    IStringScalar attrLastDataAvailable;
    IStringScalar attrEarliestDataAvailable;

    INumberScalar attrLastDataAvailableEpoch;
    INumberScalar attrEarliestDataAvailableEpoch;

    MainSplitPanel mspBpm;
    MainSplitPanel mspCorrector;

    com.toedter.calendar.JSpinnerDateEditor s0;
    com.toedter.calendar.JSpinnerDateEditor s1;
    
    FdaPositionCorectionData CurrentData;

    private boolean lock;
    private MainPanel thisis;
    SaveLoadFdaData saveLoad;
    
    FdaData globalData;
    FdaData globalDataTry;
    

    /**
     * Creates new form Main
     */
    public MainPanel() {
        thisis = this;
        
        JSmoothProgressBar progress = new JSmoothProgressBar();
        progress.setValue(0);
        Splash splash = new Splash(null, Color.WHITE, progress);
        System.out.println("Starting :" + APPLI_NAME + " " + VERSION);
        splash.setTitle(APPLI_NAME + " " + VERSION);
        //splash.initProgress();
        splash.setMaxProgress(5);
        splash.progress(1);

        MyFocusListener mfl = new MyFocusListener(this);
        this.setTitle(APPLI_NAME + " v" + VERSION);
        s0 = new com.toedter.calendar.JSpinnerDateEditor();
        JTextField jtf = ((javax.swing.JSpinner.DefaultEditor) s0.getEditor()).getTextField();
        jtf.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        jtf.addFocusListener(mfl);

        s1 = new com.toedter.calendar.JSpinnerDateEditor();
        jtf = ((javax.swing.JSpinner.DefaultEditor) s1.getEditor()).getTextField();
        jtf.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        jtf.addFocusListener(mfl);

        String fdaHost = System.getProperty("FDA_HOST");//System.getenv("FDA_HOST");
        if(fdaHost == null || !fdaHost.contains(":")){
            System.err.println("Please export FDA_HOST like \"hostname:portNumber\"");
            System.exit(-1);
        }
        
        String fdaCurrentFactor = System.getProperty("FDA_CURRENT_FACTOR");
        try{
            CurrentFactorDouble = Double.parseDouble(fdaCurrentFactor);
            System.out.println("FDA_CURRENT_FACTOR=" + String.valueOf(CurrentFactorDouble));
        }catch(NumberFormatException|NullPointerException ex){
            CurrentFactorDouble = 1.0;
            fdaCurrentFactor = null;
        }
        
        if(fdaCurrentFactor == null){
            System.out.println("Please export FDA_CURRENT_FACTOR like floting value \"3.14\"");
            System.out.println("FDA_CURRENT_FACTOR defaulting on 1.0 value");
        }
        
        fdaSaveLoadPath = System.getProperty("FDA_PATH");//System.getenv("FDA_HOST");
        if(fdaSaveLoadPath == null || fdaSaveLoadPath.isEmpty()){
            System.err.println("Please export FDA_PATH like \"/mcsvar/fdafiles\"");
            System.exit(-1);
        }
        if(!(new File(fdaSaveLoadPath)).isDirectory()){
            System.err.println("FDA_PATH:\"" + fdaSaveLoadPath + "\" is not a pointer to folder");
            System.exit(-1);
        }
        
        String host =null;
        int port = -1;
        String[] hostPort = fdaHost.split(":");
        if(hostPort.length != 2){
            System.err.println("Please export FDA_HOST like \"hostname:portNumber\"");
            System.exit(-1);
        }
        try{
            port = Integer.parseInt(hostPort[1]);
            
            InetAddress inet = InetAddress.getByName(hostPort[0]);
            inet.isReachable(1000);
            
            host = hostPort[0];
        } catch(NumberFormatException ex){
            System.err.println("Please export FDA_HOST like \"hostname:portNumber\"");
            System.exit(-1);
        } catch (UnknownHostException ex) {
            System.err.println("Host not found");
            System.err.println("Please export FDA_HOST like \"hostname:portNumber\"");
            System.exit(-1);
        } catch (IOException ex) {
            Logger.getLogger(MainPanel.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        
        try {
            fda = new FdaRequest(host, port);
        } catch (IOException ex) {
            Logger.getLogger(MainPanel.class.getName()).log(Level.SEVERE, null, ex);
        }

        initComponents();

        //attl.addErrorListener(errorHistoryFrame);
        comboPreSelet = (ComboPreSelection) jComboPreSelection;

        refreshTimeDataAvailable();

        ActionListener al = jComboDecimateFactor.getActionListeners()[0];
        jComboDecimateFactor.removeActionListener(al);
        jComboDecimateFactor.setEditable(false);
        jComboDecimateFactor.removeAllItems();
        jComboDecimateFactor.addItem("Decimate by 16386");
        jComboDecimateFactor.addItem("Decimate by 64");
        jComboDecimateFactor.addItem("Full Data");

        //new of comboManagerPreSelect for ActionListener al
        jComboDecimateFactor.addActionListener(al);
        //set the good index for getDecimateFactor()
        jComboDecimateFactor.setSelectedIndex(0);

        try {
            simpleScalarViewerLastDataAvailable.setToolTipText(DEVICE_FDA_STATUS + '/' + ATTRIBUTE_FDA_LAST_DATA_AVAILABLE);
            attrLastDataAvailable = (IStringScalar) attl.add(DEVICE_FDA_STATUS + '/' + ATTRIBUTE_FDA_LAST_DATA_AVAILABLE);
            simpleScalarViewerLastDataAvailable.setModel(attrLastDataAvailable);
            splash.progress(2);
            simpleScalarViewerEarliestDataAvailable.setToolTipText(DEVICE_FDA_STATUS + '/' + ATTRIBUTE_FDA_EARLIEST_DATA_AVAILABLE);
            attrEarliestDataAvailable = (IStringScalar) attl.add(DEVICE_FDA_STATUS + '/' + ATTRIBUTE_FDA_EARLIEST_DATA_AVAILABLE);
            simpleScalarViewerEarliestDataAvailable.setModel(attrEarliestDataAvailable);
            splash.progress(3);

            attrEarliestDataAvailableEpoch = (INumberScalar) attl.add(DEVICE_FDA_STATUS + '/' + ATTRIBUTE_FDA_EARLIEST_DATA_AVAILABLE_EPOCH);
            attrLastDataAvailableEpoch = (INumberScalar) attl.add(DEVICE_FDA_STATUS + '/' + ATTRIBUTE_FDA_LAST_DATA_AVAILABLE_EPOCH);

        } catch (ConnectionException ex) {
            ErrorPane.showErrorMessage((Component) this, "sr/d-bpm/fast_archiver_status", ex);
            Logger.getLogger(MainPanel.class.getName()).log(Level.SEVERE, null, ex);
        }

        comboPreSelet.setView(getDecimateFactor());
        comboPreSelet.setAttrTimeLastDateAvaible(attrLastDataAvailableEpoch);
        comboPreSelet.setSelectedIndex(0);

        mspBpm = new MainSplitPanel(this, true);
        mspCorrector = new MainSplitPanel(this, false);
        jTabbedPane.add("Beam Positions", mspBpm);
        jTabbedPane.add("Steerers Correction", mspCorrector);

        splash.progress(4);

        refreshAvailableDate = new Thread(new Runnable() {

            JCalendar start;

            @Override
            public void run() {
                start = startDateChooser.getJCalendar();

                while (!getThreadStop()) {
                    refreshTimeDataAvailable();

                    SwingUtilities.invokeLater(new Runnable() {

                        @Override
                        public void run() {

                            Date dateMax = new Date((long) ((getLastDataAvailable() * 1000.0) - comboPreSelet.getDurationTime()));
                            Date dateMin = new Date((long) (getEarliestDataAvailable() * 1000.0));
                            start.setMaxSelectableDate(dateMax);
                            start.setMinSelectableDate(dateMin);
                            checkAvaibleButton();
                        }
                    });

                    try {
                        Thread.sleep(3000);
                    } catch (InterruptedException ex) {
                    }
                }
            }
        });
        refreshAvailableDate.start();
        
        JMenuItem load = new JMenuItem("Open");
        load.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                LoadFrame lf = new LoadFrame(thisis);
                lf.setVisible(true);
            }
        });
        load.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O,Event.CTRL_MASK));
        jMenu1.add(load);
        
        JMenuItem save = new JMenuItem("Save");
        save.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                
                String comment = JOptionPane.showInputDialog(thisis,"Please enter comment for your Save","Comment",JOptionPane.INFORMATION_MESSAGE);
                
                if(comment != null){
                    save(comment,null);
                }
            }

        });
        save.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S,Event.CTRL_MASK));
        jMenu1.add(save);
        
        JMenuItem exit = new JMenuItem("Exit");
        exit.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
        exit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F4,Event.ALT_MASK));
        jMenu1.add(exit);

        JMenuItem errorHistory = new JMenuItem("Error history ...");
        errorHistory.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                ATKGraphicsUtils.centerFrame(jTabbedPane, ERROR_HISTORY_FRAME);
                ERROR_HISTORY_FRAME.setVisible(true);
            }
        });
        jMenu2.add(errorHistory);
        JMenuItem atkDiagnostic = new JMenuItem("Diagnostic ...");
        atkDiagnostic.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                ATKDiagnostic.showDiagnostic();
            }
        });
        jMenu2.add(atkDiagnostic);

        splash.progress(5);

        jButtonSearchActionPerformed(null);
        startup = false;
        saveLoad = new SaveLoadFdaData();
        
        this.setLocationRelativeTo(null);
        this.setVisible(true);
        splash.dispose();
    }
    
    public void save(String comment,String fileName) {
        Thread t = new SaveThread(comment,fileName);

        loadingPanel = new ThreadDlg(this, "Save in Progress", true, t);
        loadingPanel.showDlg();
    }
    
    public void loadFile(String fileName){
        Thread t = new LoadThread(fileName);
                    
        loadingPanel = new ThreadDlg(this, "Load in Progress", true, t);
        loadingPanel.showDlg();
    }

    private void actualisedLabelCurrentViewDesc(FdaData data) {
        actualisedLabelCurrentViewDesc(data, null);
    }
    private void actualisedLabelCurrentViewDesc(FdaData data,String file) {
        String label = "";
        if(file == null){
            label += "Data from Fast Data Archiver";
        } else {
            label += "Data from File";
        }
        label += " | start at : " + SDF_MILLI.format(new Date((long)data.timeStamp[0]));
        label += " | stop at : " + SDF_MILLI.format(new Date((long)data.timeStamp[data.timeStamp.length-1]));
        label += " | duation : " + data.getDuration();
        label += " | mode : " + data.decimateMode.toString();
        
        
        this.jLabelCurrentViewDesc.setText(label);
    }

    void refrechTrend(MainSplitPanel aThis, Point point) {
        if(aThis == mspBpm){
            point = pointBpmToCorrector(point);
            mspCorrector.refrechTrend(point);
        }else{
            point = pointCorrectorToBPM(point);
            mspBpm.refrechTrend(point);
        }
    }

    private Point pointBpmToCorrector(Point point) {
        point.y = (int)(point.y * FdaConstants.NB_CORRESTOR / FdaConstants.NB_BPM);
        if(point.y >= FdaConstants.NB_CORRESTOR){
            point.y -= FdaConstants.NB_CORRESTOR;
        }
        return new Point(point);
    }

    private Point pointCorrectorToBPM(Point point) {
        point.y = (int)(point.y * FdaConstants.NB_BPM / FdaConstants.NB_CORRESTOR);
        if(point.y < 0){
            point.y += FdaConstants.NB_BPM;
        }
        return new Point(point);
    }
    
    class LoadThread extends Thread{
        String fileName;
        
        public LoadThread(String fileName) {
            super();
            this.fileName = fileName;
        }

        @Override
        public void run() {
            
            try{
                FdaData data = FdaData.loadFile(fileName,loadingPanel);

                globalDataTry = data;
                
                if(globalDataTry.current != null){
                    mspBpm.CurrentSelected();
                    mspCorrector.CurrentSelected();
                }
                mspBpm.setData(globalDataTry);
                mspCorrector.setData(globalDataTry);
                
                int idx = fileName.lastIndexOf("/");
                thisis.actualisedLabelCurrentViewDesc(globalDataTry,fileName.substring(idx+1));

                globalData = globalDataTry;
                globalDataTry = null;
            }catch (FdaException ex) {
                StringWriter errors = new StringWriter();
                ex.printStackTrace(new PrintWriter(errors));

                Logger.getLogger(MainPanel.class.getName()).log(Level.SEVERE, null, ex);
                
                JOptionPane.showMessageDialog(loadingPanel,
                        ex.getMessage() + "\n" + errors.toString(),
                        "Fda Request Failed",
                        JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    public class SaveThread extends Thread{
        String comment;
        String fileName;

        SaveThread(String comment,String fileName){
            super();
            this.comment = comment;
            this.fileName = fileName;
        }

        @Override
        public void run() {
            try{
                saveLoad.saveFile(comment,fileName,globalData,loadingPanel);
            }catch(Exception ex){
                JOptionPane.showMessageDialog(loadingPanel,
                                        "Failed to write save file",
                                        "Error",
                                        JOptionPane.ERROR_MESSAGE);
            }
        }
    };

    synchronized public boolean getThreadStop() {
        return threadStop;
    }

    synchronized public void setThreadStop(boolean threadStop) {
        this.threadStop = threadStop;
        refreshAvailableDate.interrupt();
    }

    synchronized public double getLastDataAvailable() {
        return lastDataAvailable;
    }

    synchronized public void setLastDataAvailable(double lastDataAvailable) {
        this.lastDataAvailable = lastDataAvailable;
    }

    synchronized public double getEarliestDataAvailable() {
        return earliestDataAvailable;
    }

    synchronized public void setEarliestDataAvailable(double earliestDataAvailable) {
        this.earliestDataAvailable = earliestDataAvailable;
    }

    FdaRequest fda;

    private void checkAvaibleButton() {
        Date startDate = startDateChooser.getDate();
        long duration = comboPreSelet.getDurationTime();
        Date stopDate = new Date((long) (startDate.getTime() + duration));

        long newStartDate = startDate.getTime() - duration;
        long newStopDate = stopDate.getTime() + duration;

        if (attrEarliestDataAvailableEpoch != null && attrLastDataAvailableEpoch != null) {
            double earliestAvailable;
            double lastAvailable;

            earliestAvailable = attrEarliestDataAvailableEpoch.getNumber().doubleValue() * 1000;
            lastAvailable = attrLastDataAvailableEpoch.getNumber().doubleValue() * 1000;

            jButtonTimeNext.setEnabled(newStopDate <= lastAvailable);
            jButtonTimePrevious.setEnabled(newStartDate >= earliestAvailable);
        } else {
            jButtonTimeNext.setEnabled(true);
            jButtonTimePrevious.setEnabled(true);

        }
    }

    private void setDecimateView(FdaConstants.DecimateMode decimateFactor) {
        Dimension dimentionDateChoose = startDateChooser.getPreferredSize();
        Dimension dimentionSimpleViewer = simpleScalarViewerEarliestDataAvailable.getPreferredSize();

        //because full
        boolean isFullData = decimateFactor == FdaConstants.DecimateMode.FULL;
        if (isFullData) {
            if (jRadioStdDeviationData.isSelected()) {
                jRadioMeanData.setSelected(true);
            }
            dimentionDateChoose.width = 205;
            dimentionSimpleViewer.width = 205;
            startDateChooser.setDateFormatString(DATE_FORMAT_MILLI);
            //stopDateChooser.setDateFormatString(DATE_FORMAT_MILLI);
        } else {

            dimentionDateChoose.width = 175;
            dimentionSimpleViewer.width = 175;
            startDateChooser.setDateFormatString(DATE_FORMAT);
            //stopDateChooser.setDateFormatString(DATE_FORMAT);
        }

        jRadioStdDeviationData.setEnabled(!isFullData);

        startDateChooser.setPreferredSize(dimentionDateChoose);
        //stopDateChooser.setPreferredSize(dimentionDateChoose);
        startDateChooser.revalidate();
        //stopDateChooser.revalidate();

        simpleScalarViewerEarliestDataAvailable.setPreferredSize(dimentionSimpleViewer);
        simpleScalarViewerLastDataAvailable.setPreferredSize(dimentionSimpleViewer);
        simpleScalarViewerEarliestDataAvailable.revalidate();
        simpleScalarViewerLastDataAvailable.revalidate();

    }

    String getStartDate() {
        String d = ((javax.swing.JSpinner.DefaultEditor) s0.getEditor()).getTextField().getText();
        return d;
    }

    String getStopDate() {
        String d = ((javax.swing.JSpinner.DefaultEditor) s1.getEditor()).getTextField().getText();
        return d;
    }

    private void refreshTimeDataAvailable() {

        try {
            DeviceProxy dp = new DeviceProxy(DEVICE_FDA_STATUS);
            DeviceAttribute[] reply = dp.read_attribute(new String[]{"LastDataAvailableEpoch", "EarliestDataAvailableEpoch"});

            setLastDataAvailable(reply[0].extractDouble());
            setEarliestDataAvailable(reply[1].extractDouble());

        } catch (DevFailed ex) {

            Logger.getLogger(MainPanel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroupDataRequested = new javax.swing.ButtonGroup();
        errorPane1 = new fr.esrf.tangoatk.widget.util.ErrorPane();
        selPanel = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        startDateChooser = new com.toedter.calendar.JDateChooser(null, null, "HH:mm:ss  dd/MM/yyyy", s0);
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jComboPreSelection = new ComboPreSelection(startDateChooser);
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JSeparator();
        jPanel2 = new javax.swing.JPanel();
        jLabelCurrentViewDesc = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jButtonTimeNext = new javax.swing.JButton();
        jButtonSearch = new javax.swing.JButton();
        jButtonTimePrevious = new javax.swing.JButton();
        jPanel5 = new javax.swing.JPanel();
        simpleScalarViewerLastDataAvailable = new fr.esrf.tangoatk.widget.attribute.SimpleScalarViewer();
        simpleScalarViewerEarliestDataAvailable = new fr.esrf.tangoatk.widget.attribute.SimpleScalarViewer();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jRadioMeanData = new javax.swing.JRadioButton();
        jRadioStdDeviationData = new javax.swing.JRadioButton();
        jComboDecimateFactor = new javax.swing.JComboBox();
        jTabbedPane = new javax.swing.JTabbedPane();
        jMenu = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenu2 = new javax.swing.JMenu();
        jMenu3 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(1090, 783));
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        selPanel.setMinimumSize(new java.awt.Dimension(1023, 66));

        startDateChooser.setDateFormatString("dd/MM/yyyy HH:mm:ss");
        startDateChooser.setFont(new java.awt.Font("Monospaced", 0, 12)); // NOI18N
        startDateChooser.setMinimumSize(new java.awt.Dimension(175, 24));
        startDateChooser.setPreferredSize(new java.awt.Dimension(175, 25));
        startDateChooser.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                startDateChooserPropertyChange(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jLabel1.setText("Duration");

        jLabel2.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jLabel2.setText("Start");

        jComboPreSelection.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        jComboPreSelection.setSelectedIndex(-1);

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/getTimeResize.png"))); // NOI18N
        jButton1.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jButton1.setPreferredSize(new java.awt.Dimension(42, 24));
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setText("Last data");
        jButton2.setMargin(new java.awt.Insets(2, 2, 2, 2));
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addGap(23, 23, 23))
                    .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jComboPreSelection, javax.swing.GroupLayout.PREFERRED_SIZE, 157, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(startDateChooser, javax.swing.GroupLayout.PREFERRED_SIZE, 201, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton2)
                    .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(startDateChooser, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jComboPreSelection, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButton2)))
        );

        jSeparator1.setPreferredSize(new java.awt.Dimension(50, 5));

        jPanel2.setPreferredSize(new java.awt.Dimension(166, 20));

        jLabelCurrentViewDesc.setText("Error");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabelCurrentViewDesc)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jLabelCurrentViewDesc)
                .addGap(6, 6, 6))
        );

        jButtonTimeNext.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jButtonTimeNext.setText(">>");
        jButtonTimeNext.setMargin(new java.awt.Insets(2, 2, 2, 2));
        jButtonTimeNext.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonTimeNextActionPerformed(evt);
            }
        });

        jButtonSearch.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jButtonSearch.setText("Perform search");
        jButtonSearch.setMargin(new java.awt.Insets(2, 2, 2, 2));
        jButtonSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSearchActionPerformed(evt);
            }
        });

        jButtonTimePrevious.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jButtonTimePrevious.setText("<<");
        jButtonTimePrevious.setMargin(new java.awt.Insets(2, 2, 2, 2));
        jButtonTimePrevious.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonTimePreviousActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addComponent(jButtonTimePrevious, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButtonSearch, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButtonTimeNext, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jButtonTimePrevious, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jButtonSearch, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jButtonTimeNext, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        simpleScalarViewerLastDataAvailable.setText("simpleScalarViewer1");
        simpleScalarViewerLastDataAvailable.setMargin(new java.awt.Insets(2, 2, 2, 2));

        simpleScalarViewerEarliestDataAvailable.setText("simpleScalarViewer1");
        simpleScalarViewerEarliestDataAvailable.setMargin(new java.awt.Insets(2, 2, 2, 2));

        jLabel4.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jLabel4.setText("Min time");

        jLabel5.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jLabel5.setText("Max time");

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(simpleScalarViewerLastDataAvailable, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(simpleScalarViewerEarliestDataAvailable, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(0, 0, 0))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(simpleScalarViewerLastDataAvailable, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(simpleScalarViewerEarliestDataAvailable, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5)))
        );

        buttonGroupDataRequested.add(jRadioMeanData);
        jRadioMeanData.setSelected(true);
        jRadioMeanData.setText("Mean");
        jRadioMeanData.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioMeanDataActionPerformed(evt);
            }
        });

        buttonGroupDataRequested.add(jRadioStdDeviationData);
        jRadioStdDeviationData.setText("Std Deviation");
        jRadioStdDeviationData.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioStdDeviationDataActionPerformed(evt);
            }
        });

        jComboDecimateFactor.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        jComboDecimateFactor.setSelectedIndex(-1);
        jComboDecimateFactor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboDecimateFactorActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addComponent(jComboDecimateFactor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jRadioStdDeviationData)
                    .addComponent(jRadioMeanData)))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jComboDecimateFactor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jRadioMeanData))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jRadioStdDeviationData))
        );

        javax.swing.GroupLayout selPanelLayout = new javax.swing.GroupLayout(selPanel);
        selPanel.setLayout(selPanelLayout);
        selPanelLayout.setHorizontalGroup(
            selPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 1076, Short.MAX_VALUE)
            .addComponent(jSeparator1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(selPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 41, Short.MAX_VALUE)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        selPanelLayout.setVerticalGroup(
            selPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(selPanelLayout.createSequentialGroup()
                .addGroup(selPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 5, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jMenu1.setText("File");
        jMenu.add(jMenu1);

        jMenu2.setText("View");
        jMenu.add(jMenu2);

        jMenu3.setText("About");

        jMenuItem1.setText("CHANGELOG");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu3.add(jMenuItem1);

        jMenu.add(jMenu3);

        setJMenuBar(jMenu);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(selPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jTabbedPane)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jTabbedPane, javax.swing.GroupLayout.DEFAULT_SIZE, 698, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(selPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jTabbedPane.getAccessibleContext().setAccessibleName("Bpm");

        pack();
    }// </editor-fold>//GEN-END:initComponents

    public synchronized void setLock(boolean lock) {
        this.lock = lock;
    }

    public synchronized boolean getLock() {
        return this.lock;
    }

    private void jButtonSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSearchActionPerformed
        synchronized (this) {
            Thread t = new Thread(new Runnable() {

                @Override
                public void run() {
                    globalDataTry = new FdaData();

                    final Date startTime = startDateChooser.getDate();
                    final Date stopTime = new Date((long) (startTime.getTime() + comboPreSelet.getDurationTime()));
                    globalDataTry.timeStart = startTime.getTime() / 1000.0;
                    globalDataTry.timeStop = stopTime.getTime() / 1000.0;

                    globalDataTry.decimateMode = getDecimateFactor();

                    mspBpm.clearData(globalDataTry.decimateMode);
                    mspCorrector.clearData(globalDataTry.decimateMode);


                    if (globalDataTry.decimateMode != FdaConstants.DecimateMode.FULL) {
                        globalDataTry.dataTypeReq = FdaConstants.DataTypeRequest.FOUR_VALUE;
                    } else {
                        globalDataTry.dataTypeReq = FdaConstants.DataTypeRequest.MEAN;
                    }

                    globalDataTry.timeStampMode = FdaConstants.TimeStampMode.TIMESTAMP;

                    double SampleByMilliSeconde = FdaConstants.SAMPLE_FREQUENCY / 1000;
                    double deltaTimeRequestedMilliSeconde = stopTime.getTime() - startTime.getTime();
                    long nbByteAttendById = 0;

                    switch (globalDataTry.decimateMode) {
                        case FULL:
                            nbByteAttendById = (long) (SampleByMilliSeconde * deltaTimeRequestedMilliSeconde * 4.0 * 2.0);
                            break;
                        case DECIMATE64:
                            nbByteAttendById = (long) (SampleByMilliSeconde * deltaTimeRequestedMilliSeconde * 4.0 * 2.0 / 64.0);
                            break;
                        case DECIMATE256:
                            nbByteAttendById = (long) (SampleByMilliSeconde * deltaTimeRequestedMilliSeconde * 4.0 * 2.0 / 16384.0);
                            break;
                    }
                    if (globalDataTry.dataTypeReq == FdaConstants.DataTypeRequest.FOUR_VALUE) {
                        nbByteAttendById = nbByteAttendById * 4;
                    }

                    final long nbByteAttendBpm = nbByteAttendById * FdaConstants.NB_BPM;
                    final long nbByteAttendCorrector = nbByteAttendById * FdaConstants.NB_CORRESTOR;

                    globalDataTry.meanOrStdDev = jRadioMeanData.isSelected();
                    
                    try {
                        globalDataTry.current = fda.request(globalDataTry,FdaConstants.QUERY_CURRENT_BPM, 1, null, nbByteAttendCorrector, 0, true,1.0);
                    } catch (FdaException | ConnectException ex) {
                        Logger.getLogger(MainPanel.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (IOException ex) {
                        Logger.getLogger(MainPanel.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    Thread t = new Thread(new Runnable() {

                        @Override
                        public void run() {
                            try {
                                globalDataTry.position = fda.request(globalDataTry,FdaConstants.QUERY_ALL_BPM, FdaConstants.NB_BPM , loadingPanel, nbByteAttendBpm, 0);
                                
                                System.out.println("nbSample: " + globalDataTry.timeStamp.length);
                                
                                mspBpm.setData(globalDataTry);

                                if (loadingPanel != null) {
                                    loadingPanel.hideDlg();
                                }
                                thisis.actualisedLabelCurrentViewDesc(globalDataTry);
                                
                                setLock(false);
                                globalData = globalDataTry;
                                globalDataTry = null;

                            } catch (FdaException | ConnectException ex) {
                                StringWriter errors = new StringWriter();
                                ex.printStackTrace(new PrintWriter(errors));

                                Logger.getLogger(MainPanel.class.getName()).log(Level.SEVERE, null, ex);
                                if (loadingPanel != null) {
                                    loadingPanel.hideDlg();
                                }
                                JOptionPane.showMessageDialog(loadingPanel,
                                        ex.getMessage() + "\n" + errors.toString(),
                                        "Fda Request Failed",
                                        JOptionPane.ERROR_MESSAGE);
                            } catch (IOException ex) {
                                Logger.getLogger(MainPanel.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }
                    });
                    t.start();

                    t = new Thread(new Runnable() {

                        @Override
                        public void run() {
                            try {
                                globalDataTry.correction = fda.request(globalDataTry,FdaConstants.QUERY_ALL_CORRECTOR, FdaConstants.NB_CORRESTOR, null, nbByteAttendCorrector, 0);
                                
                                mspCorrector.setData(globalDataTry);
                            } catch (FdaException | ConnectException ex) {
                                Logger.getLogger(MainPanel.class.getName()).log(Level.SEVERE, null, ex);
                            } catch (IOException ex) {
                                Logger.getLogger(MainPanel.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }
                    });
                    t.start();
                    
                    //nbByteAttend =  nbByteAttendById * 96;
                }

            });
            if (!startup) {
                loadingPanel = new ThreadDlg(this, "Request in Progress", true, t);
                loadingPanel.showDlg();
            } else {
                t.start();
            }
        }
    }//GEN-LAST:event_jButtonSearchActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        setThreadStop(true);
    }//GEN-LAST:event_formWindowClosing

    private void jComboDecimateFactorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboDecimateFactorActionPerformed
        FdaConstants.DecimateMode decimate = getDecimateFactor();
        setDecimateView(decimate);
        comboPreSelet.setView(decimate);
    }//GEN-LAST:event_jComboDecimateFactorActionPerformed

    private void jRadioMeanDataActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioMeanDataActionPerformed
        boolean meanOrStdDev = jRadioMeanData.isSelected();
        mspBpm.setData(meanOrStdDev);
        mspCorrector.setData(meanOrStdDev);
    }//GEN-LAST:event_jRadioMeanDataActionPerformed

    private void jRadioStdDeviationDataActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioStdDeviationDataActionPerformed
        boolean meanOrStdDev = jRadioMeanData.isSelected();
        mspBpm.setData(meanOrStdDev);
        mspCorrector.setData(meanOrStdDev);
    }//GEN-LAST:event_jRadioStdDeviationDataActionPerformed

    private void jButtonTimeNextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonTimeNextActionPerformed
        startDateChooser.setDate(new Date((long)startDateChooser.getDate().getTime() + comboPreSelet.getDurationTime()));
        jButtonSearchActionPerformed(null);
    }//GEN-LAST:event_jButtonTimeNextActionPerformed

    private void jButtonTimePreviousActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonTimePreviousActionPerformed
        startDateChooser.setDate(new Date((long)startDateChooser.getDate().getTime() - comboPreSelet.getDurationTime()));
        jButtonSearchActionPerformed(null);
    }//GEN-LAST:event_jButtonTimePreviousActionPerformed

    private void startDateChooserPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_startDateChooserPropertyChange
        if ("date".equals(evt.getPropertyName())) {
            checkAvaibleButton();
        }
    }//GEN-LAST:event_startDateChooserPropertyChange

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        if(!mspBpm.getToggleTimeClic()){
            this.setCursor (new Cursor(Cursor.CROSSHAIR_CURSOR));
            mspBpm.setToggleTimeClic(true);
            mspCorrector.setToggleTimeClic(true);
        }else{
            mspBpm.setToggleTimeClic(false);
            mspCorrector.setToggleTimeClic(false);
            this.setCursor (new Cursor(Cursor.DEFAULT_CURSOR));
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        long duration = comboPreSelet.getDurationTime();
        //startDateChooser.set
        startDateChooser.setDate(new Date((long)((attrLastDataAvailableEpoch.getNumber().doubleValue() * 1000) - duration)));
        jButtonSearchActionPerformed(null);
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        try {
            Desktop desktop = java.awt.Desktop.getDesktop();
            URI oURL = new URI("https://gitlab.esrf.fr/accelerators/BeamDiagnostics/BeamPosition/fast-orbit-feedback/FdaReader/blob/master/CHANGELOG.md");
            desktop.browse(oURL);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private FdaConstants.DecimateMode getDecimateFactor() {
        int index = jComboDecimateFactor.getSelectedIndex();
        if (index == 0) {
            return FdaConstants.DecimateMode.DECIMATE256;
        } else if (index == 1) {
            return FdaConstants.DecimateMode.DECIMATE64;
        }
        return FdaConstants.DecimateMode.FULL;
    }

    private FdaConstants.DataTypeRequest getDataTypeRequest() {
        if (jRadioMeanData.isSelected()) {
            return FdaConstants.DataTypeRequest.MEAN;
        }
        return FdaConstants.DataTypeRequest.STD_DEVIATION;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */

        /* Create and display the form */
        MainPanel main = new MainPanel();

    }

    final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();

    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 3];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 3] = hexArray[v >>> 4];
            hexChars[j * 3 + 1] = hexArray[v & 0x0F];
            hexChars[j * 3 + 2] = '\t';
        }
        return new String(hexChars);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroupDataRequested;
    private fr.esrf.tangoatk.widget.util.ErrorPane errorPane1;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButtonSearch;
    private javax.swing.JButton jButtonTimeNext;
    private javax.swing.JButton jButtonTimePrevious;
    private javax.swing.JComboBox jComboDecimateFactor;
    private javax.swing.JComboBox jComboPreSelection;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabelCurrentViewDesc;
    private javax.swing.JMenuBar jMenu;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JRadioButton jRadioMeanData;
    private javax.swing.JRadioButton jRadioStdDeviationData;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTabbedPane jTabbedPane;
    private javax.swing.JPanel selPanel;
    private fr.esrf.tangoatk.widget.attribute.SimpleScalarViewer simpleScalarViewerEarliestDataAvailable;
    private fr.esrf.tangoatk.widget.attribute.SimpleScalarViewer simpleScalarViewerLastDataAvailable;
    private com.toedter.calendar.JDateChooser startDateChooser;
    // End of variables declaration//GEN-END:variables

    void releaseToggleTime() {
        mspBpm.setToggleTimeClic(false);
        mspCorrector.setToggleTimeClic(false);
        this.setCursor (new Cursor(Cursor.DEFAULT_CURSOR));
    }

    void setNewTime(Date time) {
        startDateChooser.setDate(time);
    }
}
