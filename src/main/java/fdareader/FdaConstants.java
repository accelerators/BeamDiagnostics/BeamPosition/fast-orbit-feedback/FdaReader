/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fdareader;

/**
 *
 * @author tappret
 */
public class FdaConstants {
//    final static int NB_BPM = 192;
//    final static int NB_BPM_BY_CELL = 6;
    final static int NB_BPM = 320;
    final static int NB_BPM_BY_CELL = 10;
//    final static int [] NB_BPM_ORDER = new int[]{1,2,4,7,9,10};
    final static int NB_CORRESTOR = 96;
    final static int NB_CORRESTOR_BY_CELL = 3;
    static double CONVERTION_FACTOR_FOR_CORRECTOR =  0.024414;
    
    final static int NB_CELL = 32;
    
    final static int START_CELL = 4;
    
    public final static int NB_SAMPLE_FFT = 2000;/*MainPanel.SAMPLE_FREQUENCY /     */

    //public final static double SAMPLE_FREQUENCY = 10144.36;
    public final static double SAMPLE_FREQUENCY = 10149.00;
    
    static final String[] LIST_CELL = new String[]{"4", "6", "8", "10", "12", "14", "16", "18", "20", "22", "24", "26", "28", "30", "32", "2"};
    static final double[] LIST_CORRECTOR_INDEX = new double[]{0, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30};
    
    static long intToLong(int buff, int buff0) {
        return ((long)buff << 32) | ((long)buff0 & 0x00000000FFFFFFFFL);
    }
    
    public enum TimeStampMode{NO_TIMESTAMP,TIMESTAMP,EXTENDED_TIMESTAMP_TE,EXTENDED_TIMESTAMP_TA};
    public enum DecimateMode{FULL,DECIMATE64, DECIMATE256};
   
    public enum DataTypeRequest {
        FOUR_VALUE(0) ,MEAN(1), MIN(2),MAX(4),STD_DEVIATION(8);

        private final int dataRequest;
        DataTypeRequest(int dataRequest) { this.dataRequest = dataRequest; }
        public int getValue() { return dataRequest; }
    };
    
    final static int SIZE_BUF = 384*1024*1024; // 1024*1024 = mo
    final static int MAX_SIZE_REQUESTABLE = ((512) * 1024*1024);
    
    public final static String END_FRAME = "\n";
    public final static String COMMAND_STATUS_SNIFFER = "CS" + END_FRAME;
    public final static String COMMAND_MAX_ID_EXIGIBLE = "CK" + END_FRAME;
    public final static String COMMAND_CURRENT_SAMPLE = "CI" + END_FRAME;
    public final static String COMMAND_CURRENT_FREQUENCY_ARCHIVER = "CF" + END_FRAME;
    public final static String COMMAND_EPOCH_LAST_DATA_AVAILABLE = "CU" + END_FRAME;
    public final static String COMMAND_EPOCH_EARLIEST_DATA_AVAILABLE = "CT" + END_FRAME;
    
    public final static double POINT_EVERY_10MICROSECOND = 1.0 / SAMPLE_FREQUENCY * 1000;
    public final static double DELTA_TIME_DECIMATE64 = POINT_EVERY_10MICROSECOND * 64;
    public final static double DELTA_TIME_DECIMATE16384 = POINT_EVERY_10MICROSECOND * 16384;
    
    public static double getSampleDeltaTime(DecimateMode decMode){
        if(decMode == DecimateMode.DECIMATE256){
            return DELTA_TIME_DECIMATE16384;
        }else if(decMode == DecimateMode.DECIMATE64){
            return DELTA_TIME_DECIMATE64;
        }
        return POINT_EVERY_10MICROSECOND;
    }
    
    private static String[] calcBPM_List(int indexCellStart) {
        String[] listBpm = new String[FdaConstants.NB_BPM];
        listBpm[0] = "tic";
        int cell = indexCellStart;
        int indexBpm = 1;
        for (int i = 0; i < FdaConstants.NB_BPM; ++i) {
            listBpm[i] = "C" + String.format("%02d", cell) + "-" + String.format("%02d", indexBpm);
            indexBpm++;
            if (indexBpm > FdaConstants.NB_BPM_BY_CELL) {
                indexBpm = 1;
                cell++;
                if (cell > FdaConstants.NB_CELL) {
                    cell = 1;
                }
            }
        }
        return listBpm;
    }

    private static String[] calcCorrector_List(int indexCellStart) {
        String[] listCorrector = new String[NB_CORRESTOR];
        listCorrector[0] = "tic";
        int cell = indexCellStart;
        int indexBpm = 1;
        for (int i = 0; i < NB_CORRESTOR; ++i) {
            listCorrector[i] = "C" + cell + "-" + indexBpm;
            indexBpm++;
            if (indexBpm > NB_CORRESTOR_BY_CELL) {
                indexBpm = 1;
                cell++;
                if (cell > NB_CELL) {
                    cell = 1;
                }
            }
        }
        return listCorrector;
    }


    public final static String[] LIST_IDS_BPM_TITLE = calcBPM_List(START_CELL);
    public final static String[] LIST_IDS_CORRECTOR_TITLE = calcCorrector_List(START_CELL);

    public final static String QUERY_CURRENT_BPM = "321";
    public final static String QUERY_ALL_BPM = "1-320";
    public final static String QUERY_ALL_CORRECTOR = 
            "412,409,407,414,416,418,415,417,419,420,422,424,421,423,425,428,430,432,429,431,433,434,436," +
            "438,435,437,439,442,444,446,443,445,447,448,450,452,449,451,453,456,458,460,457,459,461,462," +
            "464,466,463,465,467,470,472,474,471,473,475,476,478,480,477,479,481,484,486,488,485,487,489," +
            "490,492,494,491,493,495,498,500,502,499,501,503,504,506,508,505,507,509,400,402,404,401,403," +
            "405,406,408,410"
;
}
