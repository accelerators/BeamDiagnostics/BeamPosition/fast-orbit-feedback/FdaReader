/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fdareader;

import com.toedter.calendar.JDateChooser;
import java.awt.Container;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.ParseException;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import static fdareader.MainPanel.SDF;

/**
 *
 * @author tappret
 */
public class MyFocusListener implements FocusListener{

    JFrame parent;
    
    public MyFocusListener(JFrame parent) {
        this.parent = parent;
    }

    @Override
    public void focusGained(FocusEvent e) {

    }

    @Override
    public void focusLost(FocusEvent e) {
        //System.out.println(((JDateChooser)((Container)e.getSource()).getParent().getParent().getParent()).getDate());

        if (e.getSource() instanceof JTextField) {
            JTextField jtf = (JTextField) e.getSource();
            Container cont = jtf.getParent().getParent().getParent();
            if (cont instanceof JDateChooser) {
                JDateChooser jdc = (JDateChooser) cont;
                try {
                    jdc.setDate(SDF.parse(jtf.getText()));
                } catch (ParseException pe) {
                    StringWriter errors = new StringWriter();
                    pe.printStackTrace(new PrintWriter(errors));

                    //Logger.getLogger(MainPanel.class.getName()).log(Level.SEVERE, null, pe);
                    JOptionPane.showMessageDialog(parent,
                            pe.getMessage() + "\n" + errors.toString(),
                            "Error parsing Date",
                            JOptionPane.ERROR_MESSAGE);
                    jtf.setText(SDF.format(jdc.getDate()));
                    //jdc.setDate(jdc.getDate());
                }

            }
        }
    }

}
