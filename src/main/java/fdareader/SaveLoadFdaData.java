/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fdareader;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import javax.swing.JOptionPane;

/**
 *
 * @author tappret
 */
public class SaveLoadFdaData {

    void saveFile(String comment, FdaData data, ThreadDlg progress) {
        SaveLoadFdaData.this.saveFile(comment,null, data, progress);
    }
    
    void saveFile(String comment,String fileName, FdaData data,ThreadDlg progress) {

        long time0 = System.currentTimeMillis();

        if(fileName == null){
            fileName = MainPanel.fdaSaveLoadPath + "/" + (long)(data.timeStamp[0]) + "_" + data.decimateMode + "_" + data.nbSample + "_" + data.getDuration();
            if (comment != null && !comment.isEmpty()) {
                comment = comment.replaceAll("[^a-zA-Z0-9.-]", "-");
                fileName += "_" + comment;
            }
            fileName += ".zip";
        }

        File f = new File(fileName);
        boolean fileCreated = false;
        try {
            fileCreated = f.createNewFile();
            ZipOutputStream zos = new ZipOutputStream(new BufferedOutputStream(new FileOutputStream(f)));
            zos.setLevel(1);

            data.position.writeZipDescription(zos);
            int nbByteTotal = data.current.getNbByteToWrite();
            nbByteTotal += data.position.getNbByteToWrite();
            nbByteTotal += data.correction.getNbByteToWrite();

            int byteWrite = data.current.writeZip(zos, 0, nbByteTotal, progress);
            
            byteWrite += data.position.writeZip(zos, byteWrite, nbByteTotal, progress);

            data.correction.writeZip(zos, byteWrite, nbByteTotal, progress);

            zos.flush();
            zos.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(SaveLoadFdaData.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(progress,
                        ex.getMessage() + "\n" + ex.toString(),
                        "Fda Request Failed",
                        JOptionPane.ERROR_MESSAGE);
            Logger.getLogger(SaveLoadFdaData.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (progress != null) {
                progress.hideDlg();
            }
        }
        if (fileCreated) {
            System.out.println(fileName + " --> created");
        }

        long time1 = System.currentTimeMillis();

        System.out.println("write in :" + (time1 - time0) + "ms");
    }

    public final static String ZIP_DIR = "/tmp/test";
    public static final String OUTPUT_ZIP = "/tmp/output.zip";

}
