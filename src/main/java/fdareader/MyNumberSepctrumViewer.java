/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fdareader;

import fr.esrf.tangoatk.widget.attribute.NumberSpectrumViewer;
import fr.esrf.tangoatk.widget.util.chart.JLDataView;
import java.awt.Color;

/**
 *
 * @author tappret
 */
public class MyNumberSepctrumViewer extends NumberSpectrumViewer{
    
    private final JLDataView dataViewMin;
    private final JLDataView dataViewMax;
    private final JLDataView dataViewCurrent;
    private boolean addDataViewMin = false;
    private boolean addDataViewMax = false;
    private boolean addDataViewCurrent = false;
    
    public MyNumberSepctrumViewer() {
        super();
        
        dataViewMin = new JLDataView();
        dataViewMax = new JLDataView();
        dataViewCurrent = new JLDataView();
        dataViewMin.setColor(Color.GREEN);
        dataViewMax.setColor(Color.BLUE);
        dataViewCurrent.setColor(Color.cyan);
        dataViewMax.setName("Max of decimate data");
        dataViewMin.setName("Min of decimate data");
        dataViewCurrent.setName("Current");
        this.getY2Axis().setAutoScale(true);
        
    }
    
    public void resetDataViews(){
        if(addDataViewMax){
            dataViewMax.reset();
        }
        if(addDataViewMin){
            dataViewMin.reset();
        }
        if(addDataViewCurrent){
            dataViewCurrent.reset();
        }
        this.getDataView().reset();
    }
    
    public JLDataView getDataViewMin(){
        return dataViewMin;
    }
    
    public JLDataView getDataViewMax(){
        return dataViewMax;
    }

    public JLDataView getDataViewCurrent() {
        return dataViewCurrent;
    }
    
    public void addOwnDataViewMin(){
        if(!addDataViewMin){
            addDataViewMin = true;
            this.getY1Axis().addDataView(dataViewMin);
        }
    }
    
    public void removeOwnDataViewMin(){
        if(addDataViewMin){
            addDataViewMin = false;
            this.getY1Axis().removeDataView(dataViewMin);
        }
    }
    
    public void removeOwnDataView(){
        removeOwnDataViewMax();
        removeOwnDataViewMin();
    }
    
    public void addOwnDataViewMax(){
        if(!addDataViewMax){
            addDataViewMax = true;
            this.getY1Axis().addDataView(dataViewMax);
        }
    }
    
    public void removeOwnDataViewMax(){
        if(addDataViewMax){
            addDataViewMax = false;
            this.getY1Axis().removeDataView(dataViewMax);
        }
    }
    
    public void addOwnDataViewCurrent(){
        if(!addDataViewCurrent){
            addDataViewCurrent = true;
            this.getY2Axis().addDataView(dataViewCurrent);
        }
    }
    
    public void removeOwnDataViewCurrent(){
        if(addDataViewCurrent){
            addDataViewCurrent = false;
            this.getY2Axis().removeDataView(dataViewCurrent);
        }
    }
    
}
