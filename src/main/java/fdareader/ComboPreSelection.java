/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fdareader;

import com.toedter.calendar.JDateChooser;
import fr.esrf.tangoatk.core.INumberScalar;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComboBox;

/**
 *
 * @author tappret
 */
public class ComboPreSelection extends JComboBox<String> implements ActionListener{
    
    private FdaConstants.DecimateMode decimateMode;
    //private final MainPanel main;
    private final JDateChooser start;
    //private JDateChooser stop;
    INumberScalar attrTimeLastDateAvaible;
    
    
    public ComboPreSelection(JDateChooser startDate) {
        //this.main = main;
        this.start = startDate;
        //this.stop = stop;
        this.setEditable(false);
    }
    
    void setAttrTimeLastDateAvaible(INumberScalar attrTimeLastDateAvaible){
        this.removeActionListener((ActionListener)this);
        this.attrTimeLastDateAvaible = attrTimeLastDateAvaible;
        this.addActionListener((ActionListener)this);
    }
    
    void setView(FdaConstants.DecimateMode decimateMode){
        this.decimateMode = decimateMode;
        
        
        this.removeActionListener(this);
        
        this.removeAllItems();
        switch(decimateMode){
            case DECIMATE256:
                this.addItem("1 hour");
                this.addItem("4 hours");
                this.addItem("8 hours");
                this.addItem("12 hours");
                this.addItem("18 hours");
                break;
            case DECIMATE64:
                this.addItem("1 minute");
                this.addItem("2 minutes");
                this.addItem("3 minutes");
                this.addItem("4 minutes");
                this.addItem("5 minutes");
                break;
            case FULL:
                this.addItem("1 second");
                this.addItem("2 seconds");
                this.addItem("3 seconds");
                this.addItem("5 seconds");
                this.addItem("10 seconds");
                break;
        }
        this.addActionListener(this);
        
    }
    
    long getDurationTime(){

        int idx = this.getSelectedIndex();
        if(null != decimateMode)switch (decimateMode) {
            case DECIMATE256:
                switch (idx) {
                    case 0: // 1 hour
                        return 3600 * 1000;
                    case 1: // 4 hour
                        return 4 * 3600 * 1000;
                    case 2: // 8 hour
                        return 8 * 3600 * 1000;
                    case 3: // 12 hour
                        return 12 * 3600 * 1000;
                    case 4: // 18 hour
                        return 18 * 3600 * 1000;
                }
            case DECIMATE64:
                switch (idx) {
                    case 0: // 1 minute
                        return 60 * 1000;
                    case 1: // 2 minutes
                        return 60 * 2 * 1000;
                    case 2: // 3 minutes
                        return 60 * 3 * 1000;
                    case 3: // 4 minutes
                        return 60 * 4 * 1000;
                    case 4: // 5 minutes
                        return 60 * 5 * 1000;
                }
            case FULL:
                switch (idx) {
                    case 0: // 1 second
                        return 1000;
                    case 1: // 2 seconds
                        return 2000;
                    case 2: // 3 seconds
                        return 3000;
                    case 3: // 5 seconds
                        return 5000;
                    case 4: // 10 seconds
                        return 10000;
                }
            default:
                break;
        }
        try {
            throw new FdaException("ERROR no match duration Time");
        } catch (FdaException ex) {
            Logger.getLogger(ComboPreSelection.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }
    
    @Override
    public void actionPerformed(ActionEvent evt) {
        long maxDateSelectable;
        if(attrTimeLastDateAvaible != null){
            maxDateSelectable = (long)(attrTimeLastDateAvaible.getNumberScalarValue() * 1000);
        }else{
            maxDateSelectable = System.currentTimeMillis();
        }
        long duration = this.getDurationTime();
        if(start.getDate() == null){
            start.setDate(new Date(maxDateSelectable - duration));
        }
        long time = start.getDate().getTime();
        if(time + duration > maxDateSelectable){
            start.setDate(new Date(maxDateSelectable - duration));
        }
    }
    /*
    @Override
    public void actionPerformed(ActionEvent evt) {
        Object src = evt.getSource();
        if (src == this) {
            double time = 0;
            int idx = this.getSelectedIndex();
            if(decimateMode == FdaRequest.DecimateMode.DECIMATE256){
                switch (idx) {
                    case 0: // Last 15 minutes
                        time = 60 * 15;
                        break;
                    case 1: // Last 30 minutes
                        time = 60 * 30;
                        break;
                    case 2: // Last 1 hour
                        time = 3600;
                        break;
                    case 3: // Last 2 hour
                        time = 2 * 3600;
                        break;
                    case 4: // Last 4 hour
                        time = 4 * 3600;
                        break;
                    case 5: // Last 8 hour
                        time = 8 * 3600;
                        break;
                }
            }else if(decimateMode == FdaRequest.DecimateMode.DECIMATE64){
                switch (idx) {
                    case 0: // Last 30 seconds
                        time = 30;
                        break;
                    case 1: // Last 1 minute
                        time = 60;
                        break;
                    case 2: // Last 2 minutes
                        time = 60 * 2;
                        break;
                    case 3: // Last 4 minutes
                        time = 60 * 4;
                        break;
                }
            }else if(decimateMode == FdaRequest.DecimateMode.FULL){
                switch (idx) {
                    case 0: // Last 500 milliseconds
                        time = 0.5;
                        break;
                    case 1: // Last 1 second
                        time = 1;
                        break;
                    case 2: // Last 2 seconds
                        time = 2;
                        break;
                    case 3: // Last 3 seconds
                        time = 3;
                        break;
                    
                }
            }
            
            double now = main.getLastDataAvailable() * 1000.0;

            start.setDate(new Date((long) (now - time * 1000.0)));
            stop.setDate(new Date((long) now));
        }
    }*/
    
}
