# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [1.8.2] - 17-07-2024
### Changed
*  set mean as default search

## [1.8.1] - 21-07-2022
### Fixed
* Open files Improve directory listing
### Changed
*  mavenized the project

## [1.8.0] - 21-07-2022
### Changed
* Adapte fdareader for 320 bpm

## [1.7.2] - 15-03-2022
### Changed
* Current Bpm id
### Added
* Current Coef factor as env variable

## [1.7.1] - 07-10-2021
### Changed
* adapte jDataChooser size on MainFrame

## [1.7.0] - 26-06-2020

### Changed
* fix bug at the reading files on the steerers values
* change and corrected the values of the steerers as asked https://jira.esrf.fr/browse/ACU-307
* change the number of sample for FFT now it is 2000Hz instead of 500Hz

## [1.6.2] - 30-10-2019

### Changed
* add CHANGELOG in the menu "about"

## [1.6.1] - 30-10-2019

### Changed
* minor graphique change

## [1.6.0] - 29-10-2019

### Changed
* adapted mixed corrector and add FdaConstant for prepare futur change with config file

## [1.5.0] - 23-10-2019

### Changed
* adapted to ESRF EBS needs

## [1.4.0] - 20-11-2017

### Changed
* put it in gitlab
