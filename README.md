# FdaReader

Application for extract data form Fast Data Archiver (from DIAMOND Michael Abbott)

## Cloning

Any instructions or special flags required to clone the project. For example, if the project has submodules in git, then specify this and give the command to check them out correctly, example:

```
git clone git@gitlab.esrf.fr:accelerators/BeamDiagnostics/BeamPosition/fast-orbit-feedback/FdaReader.git
```

## Building and Installation

### Dependencies

The project has the following dependencies.

#### Project Dependencies

* Tango Controls 9 or higher.
* omniORB release 4 or higher.
* libzmq - libzmq3-dev or libzmq5-dev.
* ATK-Core ATK-widget
* JCalendar com.toedter 

#### Toolchain Dependencies

* javac

### Build

Instructions on building the project.

cd FdaReader
mvn package

### install

some Environement variable can be used
```
//link to Michael abbott fda server
FDA_HOST=fdaserv:8888
//path to save data files
FDA_PATH=/mcsvar/FdaFiles
// variable for current transformer factor
FDA_CURRENT_FACTOR=`cat /operation/common/appli/fdaReader/currentFactor`
//of
FDA_CURRENT_FACTOR=2.9015e-5
```

